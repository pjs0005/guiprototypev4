package com.example.locationtracker


import android.content.Context
import android.provider.MediaStore
import android.util.Log
import androidx.core.net.toUri
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File


//Upload File generator
class ServiceGenerator {
    private val BASE_URL = "https://www.onlinegdb.com/"
    // TODO: Temporary static URL, get this from frontend API code in the future
    private val API_URL = "http://192.168.1.64:8010/proxy/"

    fun uploadFile(context: Context, filename: String, desc:String ) {

        var file = File(context.filesDir,filename)

        //creating request body for file
        val requestFile: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull(), file)
        val descBody = MultipartBody.Part.createFormData("text/plain", file.name, requestFile)

        //File(file.toURI()).forEachLine { Log.d("location data",it) }
        //The gson builder
        val gson = GsonBuilder()
            .setLenient()
            .create()

        //creating retrofit object
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        //creating our api
        val api: FileUploadApi = retrofit.create(FileUploadApi::class.java)

        //creating a call and calling the upload image method
        val call = api.upload(requestFile,descBody)

        //finally performing the call
        call.enqueue(object : Callback<MyResponse?> {
            override fun onResponse(call: retrofit2.Call<MyResponse?>, response: retrofit2.Response<MyResponse?>) {
                if (!response.body()!!.error) {
                    Log.d("SUCCESS","File Uploaded Successfully...")
                } else {
                    Log.d( "Error","Some error occurred...")
                }
            }

            override fun onFailure(call: retrofit2.Call<MyResponse?>?, t: Throwable) {
                Log.d( "Error", t.message.toString())
            }
        })
    }

    fun uploadLocationBuffer(context: Context, buffer: Collection<LocationData>, jwt: String, email: String, baseUrl: String) {
        val lreq = LocationRequestBody(email, buffer);

        val gson = GsonBuilder()
                .setLenient()
                .create()

        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        //creating retrofit object
        val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        //creating our api
        val api: LocationPushApi = retrofit.create(LocationPushApi::class.java)

        val call = api.upload("Bearer $jwt", lreq)

        //finally performing the call
        call.enqueue(object : Callback<ApiResponse?> {
            override fun onResponse(call: retrofit2.Call<ApiResponse?>, response: retrofit2.Response<ApiResponse?>) {

            }

            override fun onFailure(call: retrofit2.Call<ApiResponse?>?, t: Throwable) {
                Log.d( "Error", t.message.toString())
            }
        })
    }
}