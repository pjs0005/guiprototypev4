package com.example.locationtracker;

import java.util.Collection;

public class LocationRequestBody {
    private String email;
    private Collection<LocationData> locationData;

    public LocationRequestBody(String email, Collection<LocationData> data) {
        this.email = email;
        this.locationData = data;
    }
}
