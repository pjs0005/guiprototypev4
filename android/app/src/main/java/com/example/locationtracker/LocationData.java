package com.example.locationtracker;

public class LocationData {
    private long timestamp;
    private double latitude;
    private double longitude;

    public LocationData(long timestamp, double latitude, double longitude) {
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}
