package com.example.locationtracker;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface LocationPushApi {
    @POST("location/gps-data")
    Call<ApiResponse> upload(@Header("Authorization") String authKey,
                             @Body LocationRequestBody body);
}
