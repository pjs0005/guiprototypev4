package com.nij.goals;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;

import com.getcapacitor.BridgeActivity;

import com.nij.goals.R;

public class MainActivity extends BridgeActivity {
  private int REQUEST_PERMISSION_LOCATION = 10;

  /*@Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

//    // Initializes the Bridge
//    this.init(savedInstanceState, new ArrayList<Class<? extends Plugin>>() {{
//      // Additional plugins you've installed go here
//      // Ex: add(TotallyAwesomePlugin.class);
//    }});

    LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
          buildAlertMessageNoGps();
    }

    createNotificationChannel();

    System.out.println("Checking permission for location");
    if(checkPermissionForLocation(this)) {
      System.out.println("Starting services");
//      Intent serviceIntent = new Intent(this, LocationService.class);
//      serviceIntent.putExtra("TRACKING_LOCATION_ACTIVITY", "Location");
//      serviceIntent.putExtra("TRACKING_LOCATION_STATUS", true);
//      ContextCompat.startForegroundService(this, serviceIntent);

//      Intent audioIntent = new Intent(this, AudioService.class);
//      audioIntent.putExtra("Audio", true);
//      ContextCompat.startForegroundService (this, audioIntent);
    }
    else
    {
      System.out.println("Could not start services");
    }
  }*/

  private boolean checkPermissionForLocation(Context context) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) ==
              PackageManager.PERMISSION_GRANTED)
          return true;
      else {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_PERMISSION_LOCATION);
        return false;
      }
    }
    else
      return true;
  }

  private void createNotificationChannel() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(getString(R.string.CHANNEL_ID),getString(R.string.CHANNEL_NEWS), NotificationManager.IMPORTANCE_DEFAULT );
            NotificationManager notificationManager =
                    (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }

  private void buildAlertMessageNoGps() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
            .setCancelable(false)
            .setPositiveButton("Yes", (dialog, id) -> {
                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    , 11);
            })
            .setNegativeButton("No", (dialog, id) -> {
                dialog.cancel();
                finish();
            });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
