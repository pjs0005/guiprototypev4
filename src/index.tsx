import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {AppContextProvider} from "./State";
import { BackgroundGeolocation, BackgroundGeolocationEvents, BackgroundGeolocationLocationProvider } from '@awesome-cordova-plugins/background-geolocation';
import NIJAPI from './NIJAPI';
import { isPlatform } from '@ionic/core';
import { LocalNotificationDescriptor, LocalNotifications } from '@capacitor/local-notifications';

let notifications: LocalNotificationDescriptor[] = [];

ReactDOM.render(<AppContextProvider>
    <App />
    </AppContextProvider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

if(isPlatform('android'))
{
    LocalNotifications.createChannel({
        id: 'goals-warning',
        name: 'GOALS Warnings',
        description: 'Warnings from GOALS app, e.g. nearing restriction zones',
        importance: 4,
        visibility: -1,
        sound: 'holdon.mp3',
        vibration: true
    });

    // Set up geolocation features
    document.addEventListener('deviceready', () => {
        BackgroundGeolocation.configure({
            locationProvider: BackgroundGeolocationLocationProvider.DISTANCE_FILTER_PROVIDER,
            desiredAccuracy: 0, // High accuracy
            stationaryRadius: 10,
            distanceFilter: 5,
            notificationTitle: 'Checking your area',
            notificationText: 'enabled',
            startOnBoot: true,
            interval: 5000,
            fastestInterval: 3000,
            activitiesInterval: 5000,
            startForeground: true
        }).then(() => {
            // Triggered when location change is detected
            BackgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe(location => {
                console.info(`Location update`, location);

                if(NIJAPI.instance.authenticated)
                    NIJAPI.instance.pushLocationData(location.latitude, location.longitude, 100)
                        .then(async (response) => {
                            console.log(response);

                            // Check if any violations have been detected
                            if(response.gViolationCount > 0)
                            {
                                // Create a notification warning of restriction zones
                                notifications = (await LocalNotifications.schedule({
                                    notifications: [
                                        {
                                            id: 1337,
                                            channelId: 'goals-warning',
                                            title: 'Near restricted area!',
                                            body: 'Tap to see restriction zones'
                                        }
                                    ]
                                })).notifications;
                            }
                            else
                            {
                                // Clear all location warnings if there are no current violations
                                LocalNotifications.cancel({
                                    notifications: [{ id: 1337 }]
                                });
                            }
                        });
            });

            // Triggered when Android detects the user is in a vehicle, running, still, etc.
            BackgroundGeolocation.on(BackgroundGeolocationEvents.activity).subscribe(activity => {
                console.info("Activity detected", activity);
            });
        });

        BackgroundGeolocation.start();
    }, false);
}