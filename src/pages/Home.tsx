import '@ionic/react';
import { IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonContent, IonHeader, IonIcon, IonPage, IonProgressBar, IonTitle, IonToolbar } from '@ionic/react';
import { locationOutline } from 'ionicons/icons';
import React, { useContext, useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
import { useHistory } from 'react-router';
import GOLMap from '../components/GOLMap';
import ProgressChart from '../components/ProgressChart';
import { GameContext, GamificationData } from '../gamification/gamification';
import { Task } from '../gamification/GOALS';
import './Home.css';


var taskName2 = "Sober Move";
var taskProgress = '3/7';
var taskProgressNumber = 3 / 7;
var taskDescription = "Stay sober for 7 days straight."

var lat = 34.730370;
var long = -86.586105;

interface HomeProps
{
}

// Construct the data object that will be fed into the point history chart
function constructChartData(gameData: GamificationData, maxDays=7)
{
    // Get a reversed copy of point history
    let historyData = [...gameData.pointHistory].reverse();
    //historyData.pop(); // Remove current day's point gains

    let labels: string[] = [];
    let day = new Date();
    // Move back one day
    day.setDate(day.getDate() - 1);
    // Generate labels by moving back one day at a time for each entry we have
    for(var i = 0; i < Math.min(maxDays, historyData.length); i++)
    {
        labels.unshift(`${day.getMonth() + 1}/${day.getDate()}`);
        day.setDate(day.getDate() - 1);
    }

    return {
        labels: labels,
        xLabel: 'Date',

        datasets: [{
            label: 'Points',
            data: historyData
        }]
    };
}

function mapTilerProvider(x: any, y: any, z: any) {
    return `https://a.tile.openstreetmap.org/${z}/${x}/${y}.png`
}

const GOLHome: React.FC<HomeProps> = (props: HomeProps) => {
    const [ pointsChangeStr, setPointsChangeStr ] = useState<string>("+0 (0.00%)");
    const [ pointsChangeClass, setPointsChangeClass ] = useState<string>("");

    const his = useHistory();

    const gameData = useContext(GameContext);

    const notificationPressed = () => {
        his.push('/location');
    };

    // @ts-ignore
    let tasksToday: Task[] = undefined;
    if(gameData.taskHistory && gameData.taskHistory.length > 0)
        tasksToday = gameData.taskHistory[0];

    console.log(gameData, tasksToday);

    const chartOptions = {
        scales: {
            y: {
                title: {
                    display: true,
                    text: "Points"
                }
            },
            x: {
                title: {
                    display: true,
                    text: "Date"
                }
            }
        }
    };

    // Run whenever game data updates
    useEffect(function () {
        if(!gameData) return;

        // Calculate and update point change
        if(gameData.pointHistory.length >= 1)
        {
            // Get number of points earned today
            let pointsChange = gameData.pointHistory[0];
            // Calculate percent change in points
            let percentDiff = Number((pointsChange / (gameData.points - pointsChange)) * 100).toFixed(2);

            if(pointsChange > 0)        setPointsChangeClass("positive");
            else if(pointsChange === 0) setPointsChangeClass("");
            else                        setPointsChangeClass("negative");

            setPointsChangeStr(`${pointsChange >= 0 ? '+' : '-'}${pointsChange} ${percentDiff !== 'Infinity' ? `(${percentDiff}%)` : ''}`);
        }
    }, [gameData, gameData.pointHistory, gameData.points]);

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    {/* <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons> */}
                    <IonTitle>Hi, Ethan</IonTitle>

                    <IonButtons slot="end">
                        <IonButton fill="clear" color='light' routerLink="/location">
                            <IonIcon className="titleIcons" icon={locationOutline} slot="icon-only" />
                        </IonButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen className="backgroundColor">
                {false &&
                <IonCard>
                    <IonCardHeader>
                        <IonCardTitle>
                            Five Keys Overview
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        {/* <IonGrid>
                            <IonRow>
                                <IonCol size="6">
                                    <div className="homeFiveKeyCategory">
                                        <div className="keyPoints work">
                                            <IonIcon icon={briefcaseOutline} />
                                            <span>{Math.floor(Math.random() * 250 + 50)}</span>
                                        </div>
                                        <IonLabel color="black">Meaningful Work Trajectories</IonLabel>
                                    </div>
                                </IonCol>
                                <IonCol size="6">
                                    <div className="homeFiveKeyCategory">
                                        <div className="keyPoints health">
                                            <IonIcon icon={cogOutline} />
                                            <span>{Math.floor(Math.random() * 250 + 50)}</span>
                                        </div>
                                        <IonLabel color="black">Healthy Thinking Patterns</IonLabel>
                                    </div>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol size="6">
                                    <div className="homeFiveKeyCategory">
                                        <div className="keyPoints social">
                                            <IonIcon icon={peopleCircleOutline} />
                                            <span>{Math.floor(Math.random() * 250 + 50)}</span>
                                        </div>
                                        <IonLabel color="black">Positive Social Engagement</IonLabel>
                                    </div>
                                </IonCol>
                                <IonCol size="6">
                                    <div className="homeFiveKeyCategory">
                                        <div className="keyPoints relationships">
                                            <IonIcon icon={peopleOutline} />
                                            <span>{Math.floor(Math.random() * 250 + 50)}</span>
                                        </div>
                                        <IonLabel color="black">Positive Relationships</IonLabel>
                                    </div>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol size="6" offset="3">
                                    <div className="homeFiveKeyCategory">
                                        <div className="keyPoints coping">
                                            <IonIcon icon={happyOutline} />
                                            <span>{Math.floor(Math.random() * 250 + 50)}</span>
                                        </div>
                                        <IonLabel color="black">Effective Coping Strategies</IonLabel>
                                    </div>
                                </IonCol>
                            </IonRow>
                        </IonGrid> */}
                        <ProgressChart progress={{
                            maxLevel: 5,
                            work: Math.round(Math.random() * 4) + 1,
                            health: Math.round(Math.random() * 4) + 1,
                            social: Math.round(Math.random() * 4) + 1,
                            relationships: Math.round(Math.random() * 4) + 1,
                            coping: Math.round(Math.random() * 4) + 1
                        }} />
                    </IonCardContent>
                </IonCard>
                }
                <IonCard>
                    <IonCardHeader>
                        <IonCardTitle>
                            Points Overview
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        <h1><span className="currentPoints">{gameData?.points}</span> <span className={"pointsChange " + pointsChangeClass}>{pointsChangeStr}</span></h1>

                        <Line data={constructChartData(gameData)} options={chartOptions} />

                        {tasksToday && 
                            <div className="taskOverview">
                                <h1>Today</h1>
                                {
                                    tasksToday.map(task => {
                                        let cn = "", s="+";
                                        let pt = task.point.value ?? task.point.partialValue!;
                                        if(pt > 0)      { cn = "positive"; s="+"; }
                                        else if(pt < 0) { cn = "negative"; s=""; }

                                        return (
                                            <div><span className="taskName">{task.task.name}</span><span className={"taskPoints " + cn}>{s + pt}</span><br/></div>
                                        );
                                    })
                                }
                            </div>
                        }
                    </IonCardContent>
                </IonCard>
                <IonCard routerLink="/location">
                    <IonCardHeader>
                        <IonCardTitle>
                            <h1 className="title">Your Location</h1>
                        </IonCardTitle>

                    </IonCardHeader>


                    <IonCardContent>
                        
                        {/* <Map provider={mapTilerProvider} center={[lat, long]} zoom={15} width={305} height={200}>
                            <Marker anchor={[lat, long]}  />
                        </Map> */}

                        <div style={{
                            width: 305,
                            height: 200
                        }}>
                            <GOLMap initialZoom={20} />
                        </div>

                        {/* <div className="homeMap">

                            <GOLMap />
                        </div>*/}

                    </IonCardContent>
                </IonCard>

       

                <IonCard>
                    <IonCardHeader>
                        <IonCardTitle>
                            <h1 className="title">Current Goal: {taskName2}</h1>
                        </IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                        <h2 className="taskDesc">{taskDescription}</h2>
                        <h2 className="taskDesc">Current Progress:</h2>
                        <h2 className="taskDesc">{taskProgress}</h2>
                        <IonProgressBar value={taskProgressNumber}></IonProgressBar>
                    </IonCardContent>

                </IonCard>
            </IonContent>
        </IonPage>
    );
};

export default GOLHome;