import { IonButton, IonButtons, IonContent, IonFab, IonFabButton, IonHeader, IonIcon, IonList, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { add, calendarNumberOutline } from 'ionicons/icons';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import Calendar, { CalendarTileProperties } from 'react-calendar';
import { CalendarEvent, EventTypeColors } from '../../components/calendar/CalendarTypes';
import EventItem from '../../components/calendar/EventItem';
import NIJAPI from '../../NIJAPI';
import './CalendarView.css';

function isSameDay(a: Date, b: Date)
{
    return  a.getFullYear() === b.getFullYear() &&
            a.getMonth() === b.getMonth() &&
            a.getDate() === b.getDate();
}

function eventsOnDay(date: Date, events: CalendarEvent[])
{
    return events.filter(v => {
        return isSameDay(date, new Date(v.timestamp))
    });
}

// Creates random number of dot markers for events on each day
// Should be driven by real info once data is available
function generateEventMarkers(events: CalendarEvent[])
{
    return (
        <div className="calendarEvents">
            {Array.from(events, (v, k) => {
                if(v.approval === 'rejected') return null;
                return (
                    <div key={k} className="calendarEventMarker" style={{ backgroundColor: EventTypeColors[v.type] }} />
            )})}
        </div>
    );
}

const CalendarView: React.FC = () => {
    const [value, setValue] = useState<Date>(new Date());
    const [activeStart, setActiveStart] = useState<Date>(new Date());
    const [currentEvents, setCurrentEvents] = useState<CalendarEvent[]>([]);
    const [selectedEvents, setSelectedEvents] = useState<CalendarEvent[]>([]);

    const calendarRef = useRef();

    const setDate = (date: Date) => {
        setValue(date);
        // setActiveStart(date);
    };

    // Generate dots and date selectors for a specific date
    // Utilizes useCallback so it only updates when the selected date changes
    const tileContent = useCallback(({date, view}: CalendarTileProperties) => {

        if(view === 'month')
        {
            const todayEvents = eventsOnDay(date, currentEvents);
            if(isSameDay(date, new Date())) // Current day
            {
                return (
                    <div className="calendarTileContainer">
                        <div className="calendarToday" />
                        {isSameDay(date, value) && <div className={"calendarActiveDay"} />}
                        {generateEventMarkers(todayEvents)}
                    </div>
                );
            }
            else if(isSameDay(date, value)) // Active day selector
            {
                return (
                    <div className="calendarTileContainer">
                        <div className="calendarActiveDay" />
                        {generateEventMarkers(todayEvents)}
                    </div>
                );
            }

            return (
                <div className="calendarTileContainer">
                    {generateEventMarkers(todayEvents)}
                </div>
            );
        }

        return null;
    }, [value, currentEvents]);

    // Here, events for the selected view should be retrieved from the backend
    // Currently does nothing useful
    useEffect(() => {
        NIJAPI.instance.getEventRange(new Date(activeStart.getUTCFullYear(), activeStart.getUTCMonth()), new Date(activeStart.getUTCFullYear(), activeStart.getUTCMonth() + 1))
            .then(v => {
                console.log(v);
                setCurrentEvents(v);
            });
    }, [activeStart]);

    useEffect(() => {
        setSelectedEvents(eventsOnDay(value, currentEvents));
    }, [value, currentEvents]);

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>
                        Calendar
                    </IonTitle>

                    <IonButtons slot="end">
                        <IonButton
                            fill="clear"
                            onClick={e => {
                                setDate(new Date());
                                setActiveStart(new Date());
                            }}
                        >
                            <IonIcon icon={calendarNumberOutline} slot="icon-only" />
                        </IonButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen className="ion-padding">
                <div className="calendar-view">
                    <Calendar
                        value={value}
                        activeStartDate={activeStart}
                        onChange={(date) => setDate(date as Date)}
                        onViewChange={(props) => setActiveStart(props.activeStartDate)}
                        onActiveStartDateChange={(props) => setActiveStart(props.activeStartDate)}
                        defaultView="month"
                        // showFixedNumberOfWeeks={true}
                        tileContent={tileContent}
                        minDetail="year"
                        next2Label={null}
                        prev2Label={null}
                    />
                </div>
                <div className="detail-view">
                    <IonList>
                        {selectedEvents.map(event => {
                            return <EventItem event={event} key={event.id} />
                        })}
                    </IonList>
                </div>
                <IonFab vertical="bottom" horizontal="end" slot="fixed">
                    <IonFabButton routerLink="/calendar/create">
                        <IonIcon icon={add} />
                    </IonFabButton>
                </IonFab>
            </IonContent>
        </IonPage>
    );
};

export default CalendarView;