import { IonButton, IonButtons, IonCard, IonContent, IonHeader, IonIcon, IonLabel, IonPage, IonProgressBar, IonSegment, IonSegmentButton, IonSlide, IonSlides, IonTitle, IonToolbar } from '@ionic/react';
import { giftOutline } from 'ionicons/icons';
import React, { useContext, useRef } from 'react';
import { useHistory } from 'react-router';
import TaskItem from '../../components/gamification/TaskItem';
import { GameContext } from '../../gamification/gamification';
import { Task } from '../../gamification/GOALS';
import NIJAPI from '../../NIJAPI';
import './Tasks.css';



interface TasksProps
{
}

// Filters out tasks that should not appear for today.
function filterToday(task: Task)
{
    // If no start time is given, assume it should be shown for today.
    if(task.task.date === null) return true;

    let today = new Date();
    let taskDay = new Date(task.task.date);

    // If the date string for today and the task are the same, then it should be shown for today.
    if(today.toDateString() === taskDay.toDateString()) return true;

    // If no tests pass, then do not show the task for today.
    return false;
}

const Tasks: React.FC<TasksProps> = () => {
    const gameState = useContext(GameContext);
    const { tasks, level, updateGameState, doGameEvent } = gameState;

    const slidesRef = useRef<HTMLIonSlidesElement>(null);

    const history = useHistory();

    console.log(tasks);
    
    function FinishSelfAssessment() 
    {
        //Step 1: Send dummy assessment form completion
        //Step 2: Send delete request
        //Step 3: Update list (??)

        const dummyFormData = 
        {
            educationLevel: null,
            specialty: null,
            certification: null,
            skills: null,
            internetConfidence: null,
            interviewConfidence: null,
            liftingLimit: 20,
            desiredWorkType: null,
            environmentPrefs: null,
            desiredShiftTime: null,
            previousJobCategory: null,
            transportation: null
        }

        NIJAPI.instance.patchTraits(dummyFormData)
            .then(() => NIJAPI.instance.deleteTask(6, 'finish'));

    }

    // When a task is pressed, this function will be called with the relevant task object.
    function taskInteract(task: Task)
    {
        updateGameState({
            activeTask: task
        });

        // Handle UI redirections
        if (task.associated_goals_ui === "RWAT")
            history.push('/rwat');
        else // Just finish unimplemented tasks
        {
            doGameEvent({
                type: 'finishTask',
                data: {
                }
              });
        }
    }

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="end">
                        <IonButton fill="clear" shape="round" routerLink="/prizes">
                            <IonIcon icon={giftOutline} slot="icon-only" />
                        </IonButton>
                    </IonButtons>

                    <IonTitle>Tasks</IonTitle>
                </IonToolbar>
            </IonHeader>


            <IonContent fullscreen>
                
                <iframe src={"https://i.simmer.io/@AnguisCantus/vista-test"} className="vistaFrame" />

                <IonCard className="LevelText">
                    <h5>
                        Your reward level is <span color="black">{Math.floor(level)}</span>
                        <br/>
                        You are <span color="black">{(level - Math.floor(level)) * 100}%</span> of the way to the next level
                    </h5>
                </IonCard>

                <IonProgressBar value={(level - Math.floor(level))} id="progressBar"/>

                <IonSegment onIonChange={e => console.log('Segment selected', e.detail.value)} value="today">
                    <div className="leftCol">
                    <IonSegmentButton onClick={() => {
                        slidesRef.current?.slideTo(0);
                    }} value="today">
                        <IonLabel>Today's Tasks</IonLabel>
                    </IonSegmentButton>
                    </div>
                    <div className="rightCol">
                    <IonSegmentButton onClick={() => {
                        slidesRef.current?.slideTo(1);
                    }} value="all">
                        <IonLabel>All Tasks</IonLabel>
                        </IonSegmentButton>
                    </div>
                </IonSegment>

                

                <IonSlides id="tasksSlides" ref={slidesRef} options={{
                    initialSlide: 0,
                    speed: 400,
                    autoHeight: true
                }}>
                    <IonSlide className="todayTasks">
                        <div className="todaySlide">
                            {
                                tasks.filter(filterToday).map(task => {
                                    return (
                                        <TaskItem task={task} onClick={() => {
                                            taskInteract(task);
                                        }} />
                                    );
                                })
                            }
                        </div>
                    </IonSlide>
                    <IonSlide className="allTasks">
                        <div className="allTasksSlide">
                        {
                                tasks.map(task => {
                                    return (
                                        <TaskItem task={task} onClick={() => {
                                            taskInteract(task);
                                        }} />
                                    );
                                })
                            }
                        </div>
                    </IonSlide>
                </IonSlides>
                
            </IonContent>
        </IonPage>
    );
};

export default Tasks;