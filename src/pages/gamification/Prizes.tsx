import { IonBackButton, IonButtons, IonCard, IonContent, IonHeader, IonIcon, IonPage, IonProgressBar, IonTitle, IonToolbar } from '@ionic/react';
import { timeOutline } from "ionicons/icons";
import React, { useContext, useState } from 'react';
import PrizePreview from '../../components/gamification/PrizePreview';
import RewardTrack from '../../components/gamification/RewardTrack';
import { GameContext } from '../../gamification/gamification';
import { samplePrizes } from '../../sampleObjects/samplePrizes';
import './Prizes.css';



const stampURI = "../assets/" + "ClaimedStamp.png"; 

interface PrizesProps
{
}

const Prizes: React.FC<PrizesProps> = (props: PrizesProps) => {
    const { points, level } = useContext(GameContext);
    // Points
    //let level = Math.floor(points / 10) as number; 
    let currentLevelProgress = level - Math.floor(level);
    let panelsPerPage = 2; 

    let fillPercent = 0; 
    fillPercent = currentLevelProgress / 2; 
    if (Math.floor(level) % 2 == 1)
        fillPercent += 0.5; 

    async function OnSlideChange(e: any) {
        let index = await e.target.getActiveIndex(); 
        let thisPageStartIdx = index * panelsPerPage;

        //@ts-ignore
        document.getElementById("progressBar").value = fillPercent;
        
    }

    // State
    const [incentiveShow, setIncentiveShow] = useState(false); 
    const [errandShow, setErrandShow] = useState(false);

    function renderIncentive() {
        return(
            <>
                <br/>
                <br/>
                <span className="explainText">
                    An incentive pass is an opt-in purchase that allows a temporary suspension of parole stature. 
                </span>
            </>
        )
    }

    function renderErrand() {
        return(
            <>
                <br/>
                <br/>
                <span className="explainText">
                    An errand pass is a weekly exemption from parole stature that lets you travel without restrictions.
                </span>
            </>
        )
    }

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/tasks" />
                    </IonButtons>

                    <IonTitle>Prizes</IonTitle>

                </IonToolbar>
            </IonHeader>


            <IonContent fullscreen>
            
                <RewardTrack slideReactor={OnSlideChange}  items={samplePrizes} />
                <IonProgressBar value={1} id="progressBar"/>
                <PrizePreview items = {samplePrizes} index={Math.floor(level)} />

                {/*Left time bar*/}
                 <IonCard className="timeCardLeft">
                    <IonIcon icon={timeOutline} size="large" className="timeIcon"></IonIcon>
                    <h5>
                        Your incentive passes last <span className="incentiveText">4 hours </span>
                        <span onClick={() => {incentiveShow? setIncentiveShow(false) : setIncentiveShow(true)}}>[?]</span>
                        {incentiveShow ? renderIncentive() : ''}
                    </h5>
                </IonCard>

                {/*Right time bar*/}
                <IonCard className="timeCardRight">
                    <IonIcon icon={timeOutline} size="large" className="timeIcon"></IonIcon>
                    <h5>
                        Your errand passes last <span className="errandText">30 minutes </span>
                        <span onClick={() => {errandShow ? setErrandShow(false) : setErrandShow(true)}}>[?]</span>
                        {errandShow ? renderErrand() : ''}
                    </h5>
                </IonCard>

            </IonContent>
        </IonPage>
    );
};

export default Prizes;