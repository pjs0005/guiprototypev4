import { IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';

import "./FiveKeysMenu.css";

const FiveKeysMenu: React.FC = () => {

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>
                        5-Key Model for Reentry
                    </IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen className="five-keys-content">
                <IonCard routerLink="/fivekeys/primary">
                    <IonCardHeader>
                        <IonCardTitle>
                            Primary Assessments
                        </IonCardTitle>
                    </IonCardHeader>

                    <IonCardContent>
                        Primary assessments include RWAT (Reentry Wellbeing Assessment Tool), Biopsychosocial Assessment, and Cultural Ecogram. In order to start working along different keys, please complete the primary assessments.
                    </IonCardContent>
                </IonCard>

                <IonCard routerLink="/fivekeys/case">
                    <IonCardHeader>
                        <IonCardTitle>
                            Case Conceptualization
                        </IonCardTitle>
                    </IonCardHeader>

                    <IonCardContent>
                        Case Conceptualization includes assigned sessions by the program specialist after discussing with you. These are the interventions that help you improve along different keys. If you have already completed primary assessments, you should be able to see the list of assigned interventions along different keys.
                    </IonCardContent>
                </IonCard>
            </IonContent>
        </IonPage>
    )
};

export default FiveKeysMenu;