import { IonBackButton, IonButtons, IonContent, IonHeader, IonItem, IonLabel, IonList, IonNote, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React, { useState } from 'react';

import "./PrimaryAssessments.css";

interface AssessmentStatus {
    status: "done" | "pending",
    time: Date
};

interface Assessment {
    name: string,
    description: string,
    routerLink?: string,
    status: AssessmentStatus
};

const StatusNote: React.FC<{status: AssessmentStatus}> = ({status}) => {
    if(status.status === "done")
        return (
            <IonNote color="success" slot="end">
                Done
            </IonNote>
        );
    
    return (
        <IonNote color="warning" slot="end">
            Pending
            <br />
            {status.time.toLocaleDateString()}
        </IonNote>
    );
}

const PrimaryAssessments: React.FC = () => {
    const d = new Date();
    d.setDate(d.getDate() + 7);
    const [items, setItems] = useState<Assessment[]>([
        {
            name: "Reentry Well-being Assessment",
            routerLink: "/rwat",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consequat pharetra sem at luctus. Sed lacinia risus eu mauris hendrerit, in efficitur orci viverra. Donec sodales sed sapien dapibus tempus. Nam et mi eget dui ullamcorper dictum a eu sapien. In bibendum nisl eu turpis pharetra, eu maximus elit venenatis.",
            status: {
                status: "pending",
                time: new Date()
            }
        },
        {
            name: "Biopsychosocial Assessment",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consequat pharetra sem at luctus. Sed lacinia risus eu mauris hendrerit, in efficitur orci viverra. Donec sodales sed sapien dapibus tempus. Nam et mi eget dui ullamcorper dictum a eu sapien. In bibendum nisl eu turpis pharetra, eu maximus elit venenatis.",
            status: {
                status: "done",
                time: new Date()
            }
        },
        {
            name: "Cultural Ecogram",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consequat pharetra sem at luctus. Sed lacinia risus eu mauris hendrerit, in efficitur orci viverra. Donec sodales sed sapien dapibus tempus. Nam et mi eget dui ullamcorper dictum a eu sapien. In bibendum nisl eu turpis pharetra, eu maximus elit venenatis.",
            status: {
                status: "pending",
                time: d
            }
        }
    ]);

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/fivekeys" />
                    </IonButtons>

                    <IonTitle>
                        Primary Assessments
                    </IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <IonList>
                    {
                        items.map((v, i) => 
                            <IonItem
                                button
                                routerLink={v.routerLink}
                                className="assessment-item"
                                onClick={e => {
                                    let it = [...items];
                                    it[i].status.status = "done";
                                    it[i].status.time = new Date();
                                    setItems(it);
                                }}
                            >
                                <IonLabel>
                                    <b className="assessment-title">{v.name}</b>
                                    <br />
                                    <p className="assessment-description">{v.description}</p>
                                </IonLabel>

                                <StatusNote status={v.status} />
                            </IonItem>
                        )
                    }
                </IonList>
            </IonContent>
        </IonPage>
    );
};

export default PrimaryAssessments;