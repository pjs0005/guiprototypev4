import { IonBackButton, IonButtons, IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonNote, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import { build, heart, people, personAdd, repeat } from 'ionicons/icons';
import React from 'react';
import "./CaseConcept.css";


const fiveKeyItems: {name: string, icon: string}[] = [
    {
        name: "Meaningful Work Trajectories",
        icon: build
    },
    {
        name: "Effective Coping Strategies",
        icon: repeat
    },
    {
        name: "Positive Social Engagement",
        icon: people
    },
    {
        name: "Healthy Thinking Patterns",
        icon: heart
    },
    {
        name: "Positive Relationships",
        icon: personAdd
    }
]

const CaseConcept: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/fivekeys" />
                    </IonButtons>

                    <IonTitle>
                        Case Conceptualization
                    </IonTitle>
                </IonToolbar>
            </IonHeader>

            <IonContent fullscreen>
                <IonList>
                    {
                        fiveKeyItems.map(v => 
                            <>
                                <IonItem className="key-item" button lines="none">
                                    <IonIcon icon={v.icon} slot="start" />
                                    <IonLabel>
                                        {v.name}
                                    </IonLabel>
                                    <IonNote color="success">
                                        0 to do
                                    </IonNote>
                                </IonItem>
                                <IonItem className="key-description">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consequat pharetra sem at luctus. Sed lacinia risus eu mauris hendrerit, in efficitur orci viverra.
                                </IonItem>
                            </>
                        )
                    }
                </IonList>
            </IonContent>
        </IonPage>
    );
};

export default CaseConcept;