import { IonButton, IonCard, IonCardTitle, IonCol, IonContent, IonIcon, IonImg, IonInput, IonItem, IonLabel, IonList, IonModal, IonPage, IonRow, IonText, IonToast } from '@ionic/react';
import { callOutline, mailOutline } from 'ionicons/icons';
import React, { useContext, useState } from 'react';
//import { setIsLoggedIn, setUsername } from '../data/user/user.actions';
//import { connect } from '../data/connect';
import { RouteComponentProps } from 'react-router';
import { GameContext } from '../gamification/gamification';
import NIJAPI from '../NIJAPI';
import { AppContext } from '../State';
import './Login.css';

interface OwnProps extends RouteComponentProps { }




const Login: React.FC= () => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [formSubmitted, setFormSubmitted] = useState(false);
    const [usernameError, setUsernameError] = useState(false);
    const [passwordError, setPasswordError] = useState(false);
    const [showModal, setShowModal] = useState(false);
    const [toastText, setToast] = useState('');

    const { state, dispatch } = useContext(AppContext);

    const { updateGameState } = useContext(GameContext);

    const api = NIJAPI.instance;

    const login = async (e: React.FormEvent) => {
        e.preventDefault();

        setFormSubmitted(true);
        if (!username) {
            setUsernameError(true);
        }
        if (!password) {
            setPasswordError(true);
        }

        if(!username || !password) return;

        api.authenticate(username, password).then((r) => {
            setToast("Login Success");
            dispatch({
                type: 'setNameOfUser',
                nameOfUser: username
            });

            updateGameState({
                username: username,
                userId: api.userId
            });

            // Clear form
            setUsernameError(false);
            setPasswordError(false);

            setUsername('');
            setPassword('');
        }).catch((e: string) => {
            setToast("Login Failed: " + e);
        });
    };

    return (
        <IonPage id="login-page">
          
                 <IonContent className="backgroundColor">
                <p><br/><br/></p>
                <IonImg className="appTitle" src="../assets/goals.png" />
                <p><br/></p>
                <IonImg className="logo" src="../assets/schoollogos.png" />
                <p><br /></p>
                <IonCard>
                <form noValidate onSubmit={login}>
                    <IonList>
                        <IonItem>
                            <IonLabel position="stacked" color="primary">Username</IonLabel>
                            <IonInput name="username" type="text" value={username} spellCheck={false} autocapitalize="off" onIonChange={e => setUsername(e.detail.value!)}
                                required>
                            </IonInput>
                        </IonItem>

                        {formSubmitted && usernameError && <IonText color="danger">
                            <p className="ion-padding-start">
                                Username is required
              </p>
                        </IonText>}

                        <IonItem>
                            <IonLabel position="stacked" color="primary">Password</IonLabel>
                            <IonInput name="password" type="password" value={password} onIonChange={e => setPassword(e.detail.value!)}>
                            </IonInput>
                        </IonItem>

                        {formSubmitted && passwordError && <IonText color="danger">
                            <p className="ion-padding-start">
                                Password is required
              </p>
                        </IonText>}
                    </IonList>

                    <IonRow>
                        <IonCol>
                            <IonButton type="submit" expand="block">Login</IonButton>
                        </IonCol>
                        <IonCol>
                            <IonButton onClick={() => setShowModal(true)} color="light" expand="block">Help</IonButton>
                        </IonCol>
                    </IonRow>
                    </form>
                    </IonCard>

                <IonModal isOpen={showModal} cssClass='my-custom-class'>
                    <h1 className="missionCategory">Help</h1>
                    <IonImg className="houseImg" src="../assets/questionmark.png" />
                    <IonCard>
                        <IonCardTitle className="helpText">Tech Support</IonCardTitle>
                        <p className="helpText"><IonIcon icon={mailOutline} />techsupport@gameoflife.com</p>
                        <p className="helpText"><IonIcon icon={callOutline} />555-555-5555</p>
                    </IonCard>
                    
                    <IonButton onClick={() => setShowModal(false)} fill="clear">Close</IonButton>
                </IonModal>

                {/* Temporary means of providing feedback for login success/failure */}
                <IonToast
                    isOpen={ toastText !== "" }
                    onDidDismiss={ () => setToast("")}
                    message={toastText}
                    duration={3000}
                    position="bottom"
                />
            </IonContent>

        </IonPage>
    );
};

export default Login;