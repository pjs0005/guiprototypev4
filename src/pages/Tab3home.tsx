import { IonAlert, IonButton, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonImg, IonInput, IonItem, IonItemDivider, IonLabel, IonList, IonModal, IonPage, IonRow, IonSegment, IonSegmentButton, IonSlide, IonSlides, IonTitle, IonToolbar } from '@ionic/react';
import { briefcaseOutline, cogOutline, happyOutline, peopleCircleOutline, peopleOutline } from 'ionicons/icons';
import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router';
import TaskItem from '../components/TaskItem';
import { sampleTasks } from '../sampleObjects/sampleTasks';
import { AppContext } from '../State';
import './Tab3home.css';



const slideOpts = {
    initialSlide: 0,
    speed: 400,
    autoHeight: true,
    
};




const Tab3home: React.FC = () => {
    const [showModal, setShowModal] = useState(false);
    const [showModal2, setShowModal2] = useState(false);
    const [showModal3, setShowModal3] = useState(false);
    const [showAlert1, setShowAlert1] = useState(false);
    const [showAlert2, setShowAlert2] = useState(false);
    const [showAlert3, setShowAlert3] = useState(false);
    const [text, setText] = useState<string>();
    const [text2, setText2] = useState<string>();
    const [text3, setText3] = useState<string>();
    const [text4, setText4] = useState<string>();
   
    // @ts-ignore
    const { state, dispatch } = useContext(AppContext);

    const history = useHistory();
    
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Tasks</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent className="backgroundColor">
                
                <div className="parentImage">
                    <IonImg className="islandImg" src="../assets/houseboat.png" />
                    
                    
                </div>

                
       
                
                {/*-- Segment with default selection --*/}
                <IonSegment onIonChange={e => console.log('Segment selected', e.detail.value)} value="today">
                    <div className="leftCol">
                    <IonSegmentButton onClick={() => {
                        //@ts-ignore
                        document.getElementById("tab3homeslides").slideTo(0);
                    }} value="today">
                        <IonLabel>Today's Tasks</IonLabel>
                    </IonSegmentButton>
                    </div>
                    <div className="rightCol">
                    <IonSegmentButton onClick={() => {
                        //@ts-ignore
                        document.getElementById("tab3homeslides").slideTo(1);
                    }} value="all">
                        <IonLabel>All Tasks</IonLabel>
                        </IonSegmentButton>
                    </div>

                </IonSegment>

                <IonSlides id="tab3homeslides" options={slideOpts}>
                    <IonSlide>
                        <div className="slide1">
                            {
                                sampleTasks.map(task => {
                                    return (
                                        <TaskItem task={task} onClick={e => {
                                            if(task.task.taskId === 7)
                                                history.push("/rwat");
                                        }} />
                                    );
                                })
                            }
                            {/* <Rtif boolean={state.taskW1}>
                                <IonButton onClick={() => setShowModal(true)} expand='block' color='light' className="taskButtons"><div className="buttonLeftCol">Complete Job Questionnaire</div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;25 <IonIcon className="iconPicWork" icon={constructOutline} /></IonButton>
                            </Rtif>

                            <Rtif boolean={state.taskW2}>
                                <IonButton onClick={() => setShowModal2(true)} expand='block' color='light' className="taskButtons"><div className="buttonLeftCol">Visit a Job Site</div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;15 <IonIcon className="iconPicWork" icon={constructOutline} /></IonButton>
                            </Rtif>
                            <Rtif boolean={state.taskH1}>
                                <IonButton onClick={() => setShowModal3(true)} expand='block' color='light' className="taskButtons"><div className="buttonLeftCol">Meditate for 3 Minutes</div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;25 <IonIcon className="iconPicHealth" icon={flashOutline} /></IonButton>
                            </Rtif>
                            <IonButton routerLink="/rwat" expand='block' color='light' className="taskButtons"><div className="buttonLeftCol">Complete RWAT</div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;25 <IonIcon className="iconPicHealth" icon={flashOutline} /></IonButton> */}
                        </div>

                    </IonSlide>
                    <IonSlide>
                        <IonGrid>
                            <IonRow>
                                <IonCol size="6">
                                    {/* <IonButton expand='block' color="tertiary" size="large" className="allTaskButton" routerLink="/taskswork"><div className="allButtonTextWork">Work</div><IonIcon className="allTaskButtonIconWork" icon={hammerOutline}/></IonButton> */}
                                    <IonButton
                                        expand='block'
                                        size='large'
                                        className='allTaskButton work'
                                        routerLink='/taskswork'
                                        fill='outline'
                                    >
                                        <div className="categoryButtonContents">
                                            <IonIcon icon={briefcaseOutline} />
                                            <IonLabel color="black">Meaningful Work Trajectories</IonLabel>
                                        </div>
                                    </IonButton>
                                </IonCol>
                                <IonCol size="6">

                                    {/* <IonButton expand="block" color="secondary" size="large" className="allTaskButton" routerLink="/taskshealth"><div className="allButtonTextHealth">Health</div><IonIcon className="allTaskButtonIconHealth" icon={medkitOutline} /></IonButton> */}
                                    <IonButton
                                        expand='block'
                                        size='large'
                                        className='allTaskButton health'
                                        routerLink='/taskshealth'
                                        fill='outline'
                                    >
                                        <div className="categoryButtonContents">
                                            <IonIcon icon={cogOutline} />
                                            <IonLabel color="black">Healthy Thinking Patterns</IonLabel>
                                        </div>
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol size="6">

                                    {/* <IonButton expand="block" color="warning" size="large" className="allTaskButton"><div className="allButtonTextComm">Community</div><IonIcon className="allTaskButtonIconComm" icon={earthOutline} /></IonButton> */}
                                    <IonButton
                                        expand='block'
                                        size='large'
                                        className='allTaskButton social'
                                        routerLink='/taskswork'
                                        fill='outline'
                                    >
                                        <div className="categoryButtonContents">
                                            <IonIcon icon={peopleCircleOutline} />
                                            <IonLabel color="black">Positive Social Engagement</IonLabel>
                                        </div>
                                    </IonButton>
                                </IonCol>
                                <IonCol size="6">

                                    {/* <IonButton expand="block" color="danger" size="large" className="allTaskButton"><div className="allButtonTextLaw">Law</div><IonIcon className="allTaskButtonIconLaw" icon={handLeftOutline} /></IonButton> */}
                                    <IonButton
                                        expand='block'
                                        size='large'
                                        className='allTaskButton relationships'
                                        routerLink='/taskswork'
                                        fill='outline'
                                    >
                                        <div className="categoryButtonContents">
                                            <IonIcon icon={peopleOutline} />
                                            <IonLabel color="black">Positive Relationships</IonLabel>
                                        </div>
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol className="centerCol" size="6" offset="3">
                                    <IonButton
                                        expand='block'
                                        size='large'
                                        className='allTaskButton coping'
                                        routerLink='/taskswork'
                                        fill='outline'
                                    >
                                        <div className="categoryButtonContents">
                                            <IonIcon icon={happyOutline} />
                                            <IonLabel color="black">Effective Coping Strategies</IonLabel>
                                        </div>
                                    </IonButton>
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                        
                    </IonSlide>
                </IonSlides>



                <IonModal isOpen={showModal} cssClass='my-custom-class'>
                    <h1 className="missionCategory">Complete a Job Questionnaire</h1>
                    <IonList>
                        
                        <IonItemDivider>What are you interested in?</IonItemDivider>
                        <IonItem>
                            <IonInput value={text} placeholder="Enter Input" onIonChange={e => setText(e.detail.value!)} clearInput></IonInput>
                        </IonItem>
                        <IonItemDivider>Would you prefer to work inside or outside?</IonItemDivider>
                        <IonItem>
                            <IonInput value={text2} placeholder="Enter Input" onIonChange={e => setText2(e.detail.value!)} clearInput></IonInput>
                        </IonItem>
                        <IonItemDivider>Are you confident using computers?</IonItemDivider>
                        <IonItem>
                            <IonInput value={text3} placeholder="Enter Input" onIonChange={e => setText3(e.detail.value!)} clearInput></IonInput>
                        </IonItem>
                        <IonItemDivider>Do you enjoy interacting with people?</IonItemDivider>
                        <IonItem>
                            <IonInput value={text4} placeholder="Enter Input" onIonChange={e => setText4(e.detail.value!)} clearInput></IonInput>
                        </IonItem>

                    </IonList>
                    <IonButton color="success" onClick={() => setShowAlert1(true)}>Submit</IonButton>
                    <IonAlert
                        isOpen={showAlert1}
                        onDidDismiss={() => {
                            setShowAlert1(false);
                            dispatch({
                                type: 'setTaskW1',
                                taskW1: false
                            });
                            dispatch({
                                type: 'setTaskW3',
                                taskW1: true
                            });
                            dispatch({
                                type: 'setWood',
                                wood: state.wood + 25
                            })
                            setShowModal(false);
                            }}
                        cssClass='my-custom-class'
                        header={'Good Job!'}

                        message={'Press "Confirm" to confirm your completion of the mission.'}
                        buttons={['Confirm']}
                    />
                    <IonButton onClick={() => setShowModal(false)} fill="clear">Close</IonButton>
                </IonModal>

                <IonModal isOpen={showModal2} cssClass='my-custom-class'>
                    <h1 className="missionCategory">Visit a Job Site</h1>
                    <IonImg className="houseImg" src="../assets/job.png" />
                    <IonButton color="success" onClick={() => setShowAlert2(true)}>Start</IonButton>
                    <IonAlert
                        isOpen={showAlert2}
                        onDidDismiss={() => {
                            setShowAlert2(false);
                            dispatch({
                                type: 'setTaskW2',
                                taskW2: false
                            });
                            dispatch({
                                type: 'setWood',
                                wood: state.wood + 15
                            })
                            setShowModal2(false);
                        }}
                        cssClass='my-custom-class'
                        header={'Good Job!'}

                        message={'Your mission will complete as soon as your case worker approves the results.'}
                        buttons={['Confirm']}
                    />
                    <IonButton onClick={() => setShowModal2(false)} fill="clear">Close</IonButton>
                </IonModal>

                <IonModal isOpen={showModal3} cssClass='my-custom-class'>
                    <h1 className="missionCategory">Meditate for 3 minutes</h1>
                    <IonImg className="houseImg" src="../assets/clock.jpg" />
                    <IonButton color="secondary" onClick={() => setShowAlert1(true)}>Start</IonButton>
                    <IonAlert
                        isOpen={showAlert3}
                        onDidDismiss={() => {
                            setShowAlert3(false);
                            dispatch({
                                type: 'setTaskH1',
                                taskH1: false
                            });
                            dispatch({
                                type: 'setElec',
                                elec: state.elec + 25
                            })
                            setShowModal3(false);
                        }}
                        cssClass='my-custom-class'
                        header={'Good Job!'}

                        message={'Press "Confirm" to confirm your completion of the mission.'}
                        buttons={['Confirm']}
                    />
                    <IonButton onClick={() => setShowModal3(false)} fill="clear">Close</IonButton>
                </IonModal>
                
                <div id="X"></div>
          
            </IonContent>
        </IonPage>
    );
};

export default Tab3home;