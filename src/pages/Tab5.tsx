import { IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonContent, IonGrid, IonHeader, IonImg, IonPage, IonRow, IonTitle, IonToolbar } from '@ionic/react';
import { hammerOutline } from 'ionicons/icons';
import React from 'react';
import './Tab3.css';

const Tab3: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Improvements</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 3</IonTitle>
          </IonToolbar>
              </IonHeader>
              <IonGrid>
                  <IonRow>
                      <IonCol>
                          <IonCard>
                              <IonCardHeader>
                                  <IonCardTitle>
                                      <p>Work</p>
                                  </IonCardTitle>
                              </IonCardHeader>
                              <IonCardContent>
                                  <p></p>
                              </IonCardContent>
                          </IonCard>
                      </IonCol>
                      <IonCol>
                          <IonCard>
                              <IonCardHeader>
                                  <IonCardTitle>
                                      <p>Health</p>
                                  </IonCardTitle>
                              </IonCardHeader>
                              <IonCardContent>
                                  <p></p>
                              </IonCardContent>
                          </IonCard>
                      </IonCol>
                  </IonRow>
                  <IonRow>
                      <IonCol>
                          <IonCard>
                              <IonCardHeader>
                                  <IonCardTitle>
                                      <p>Family</p>
                                  </IonCardTitle>
                              </IonCardHeader>
                              <IonCardContent>
                                  <p></p>
                              </IonCardContent>
                          </IonCard>
                      </IonCol>
                      <IonCol>
                          <IonCard>
                              <IonCardHeader>
                                  <IonCardTitle>
                                      <p>Community</p>
                                  </IonCardTitle>
                              </IonCardHeader>
                              <IonCardContent>
                                  <p></p>
                              </IonCardContent>
                          </IonCard>
                      </IonCol>
                  </IonRow>
                  <IonRow>
                      <IonCol>
                          <IonCard>
                              <IonCardContent>
                                  <div className="pointtotal">
                                      <h1>1340</h1>
                                      <div className="hammericon">
                                      <IonImg src={hammerOutline} />
                                          </div>
                                      </div>
                              </IonCardContent>
                          </IonCard>
                      </IonCol>
                  </IonRow>
              </IonGrid>


      </IonContent>
    </IonPage>
  );
};

export default Tab3;
