import {
  IonButton,
  IonCard,
  IonContent,
  IonHeader, IonPage,
  IonRefresher,
  IonRefresherContent,
  IonSlide,
  IonSlides,
  IonText,
  IonTitle,
  IonToast,
  IonToolbar
} from '@ionic/react';
import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router';
import SectionRadio from '../../components/rwat/SectionRadio';
import SectionSocial from '../../components/rwat/SectionSocial';
import SectionText from '../../components/rwat/SectionText';
import SectionTime from '../../components/rwat/SectionTime';
import { getPages, getPagesObj, Section } from '../../data/questions';
import { GameContext } from '../../gamification/gamification';
import NIJAPI from '../../NIJAPI';
import { Rtif } from '../../rtif';
import RWAT from '../../RWAT';
import './Home.css';


const Home: React.FC = () => {
  // When true, always allow user to go to next section, even if the current page is incomplete
  const validOverride = true;

  const [dummy, setDummy] = useState<boolean>(false);
  const [showToast, setToast] = useState<boolean>(false);
  const rwatObj = RWAT.getInstance();

  const history = useHistory();

  const gameState = useContext(GameContext);
  const { activeTask, doGameEvent, stateFlags } = gameState;

  console.log("rwat", gameState);

  // useEffect(() => {
  //   // @ts-ignore
  //   document.getElementById("rwat")?.swiper?.update();
  //     // .then(v => { v.update(); console.log(v) });

  //   console.log(document.getElementById("rwat"));
  // });

  const refresh = (e: CustomEvent) => {
    setTimeout(() => {
      e.detail.complete();
    }, 3000);
  };

  const getSection = (section: Section, index: any) => {
    let props = {
      onChange: undefined,
      // @ts-ignore
      onValidChange: () => { setDummy(!dummy); },
      onSpecialChange: undefined,
      id: section?.id,
      name: section?.name,
      prompt: section?.prompt,
      questions: section?.questions,
      labels: section?.labels,
      values: section?.values,
      category: section?.category,
      disabled: false,
      key: index
    };

    Object.assign(props, section);

    if((props.id === 'mostTimePerson_1' || props.id === 'mostTimePerson_2') && rwatObj.getSection('mostTimePerson_0')?.getQuestionAnswers()[0] === 1)
      props.disabled = true;

    if(props.id === 'mostTimePerson_0')
      // @ts-ignore
      props.onChange = () => { setDummy(!dummy); };

    if(props.id === 'comAndLeisure')
      // @ts-ignore
      props.onSpecialChange = () => {
        // @ts-ignore
        setTimeout(() => document?.getElementById("rwat")?.updateAutoHeight(), 0);
      };

    if(section.type === 'radio')
      return <SectionRadio {...props} />
    if(section.type === 'time')
      return <SectionTime {...props} />
    if(section.type === 'text')
      return <SectionText {...props} />
    if(section.type === 'socialnetwork')
      return <SectionSocial {...props} />

    return null;
  }

  return (
    <IonPage id="home-page">
      <IonHeader>
        <IonToolbar>
          <IonTitle>RWAT</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen class="ion-padding">
        <IonRefresher slot="fixed" onIonRefresh={refresh}>
          <IonRefresherContent></IonRefresherContent>
        </IonRefresher>

        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">
              RWAT
            </IonTitle>
          </IonToolbar>
        </IonHeader>

        {/* <IonList>
          {messages.map(m => <MessageListItem key={m.id} message={m} />)}
        </IonList> */}
        {/* <SectionRadio name="Healthy Thinking Patterns" prompt={prompt} questions={questions} labels={labelsAgree4} />
        <SectionRadio name="Healthy Thinking Patterns" prompt={prompt} questions={questions} labels={labels5} values={[0,1,2,3,4]} />
        <SectionRadio name="Healthy Thinking Patterns" prompt={prompt} questions={questions} labels={labels7Test}/>
        <SectionRadio7 name="Healthy Thinking Patterns" prompt={prompt} questions={questions} labels={labels7} /> */}

        <IonSlides scrollbar={true} id="rwat" options={{
              initialSlide: 0,
              speed: 400,
              autoHeight: true,
              allowTouchMove: false,
              observer: true,
              observeSlideChildren: true,

              onSlideChangeEnd: swiperObj => {
                //@ts-ignore
                
              }
          }} onIonSlideTransitionEnd={() => {
            //@ts-ignore
            document.getElementById("rwat")?.parentElement?.scrollToTop();
          }}>
          {
            getPagesObj().map((page, index, arr) => {
              let valid = rwatObj.validatePage(getPages()[index]);
              return (
                <IonSlide key="index">
                  <IonCard>
                      {
                        page.map((s, i) => {
                          return getSection(s as Section, i);
                        })
                      }
                      <Rtif boolean={valid !== ''}>
                        <IonText color="warning" style={{ whiteSpace: 'pre-line' }}>{valid}</IonText>
                      </Rtif>
                      <Rtif boolean={index !== 0}>
                        <IonButton onClick={() => {
                          // @ts-ignore
                          document.getElementById("rwat").slidePrev();
                        }} expand="block">Back</IonButton>
                      </Rtif>
                      <Rtif boolean={index !== arr.length-1}>
                        <IonButton disabled={!validOverride && valid !== ''}
                          onClick={() => {
                            // @ts-ignore
                            document.getElementById("rwat").slideNext(); 
                          }} expand="block">Next</IonButton>
                      </Rtif>
                      <Rtif boolean={index === arr.length-1 || stateFlags["rwatQuickSubmit"]}>
                        <IonButton onClick={() => {
                          console.log("submit");

                          // Finish regardless of RWAT submission result
                          if(stateFlags["rwatAlwaysFinish"])
                          {
                            doGameEvent({
                              type: 'finishTask',
                              data: {}
                            });
  
                            history.goBack();
                          }

                          NIJAPI.instance.pushRwat()
                            .then(v => {
                              console.log("rwat success");

                              // // Send signal that the active task is completed
                              // doGameEvent({
                              //   type: 'finishTask',
                              //   data: {}
                              // });

                              if(!stateFlags["rwatAlwaysFinish"])
                                history.goBack();
                            })
                            .catch(e => {
                              setToast(true);
                              console.error(e);
                            });
                        }} expand="block">Submit</IonButton>
                      </Rtif>
                  </IonCard>
                </IonSlide>
              )})
          }

          { console.log(RWAT.getInstance().getIDs(), RWAT.getInstance()) }
        </IonSlides>

        <IonToast
          isOpen={showToast}
          onDidDismiss={() => setToast(false)}
          message="Error submitting RWAT. Make sure all questions are answered!"
          position="bottom"
          duration={3000}
        />
      </IonContent>
    </IonPage>
  );
};

export default Home;
