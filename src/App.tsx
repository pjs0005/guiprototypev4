import React, { useContext, useEffect, useRef, useState } from 'react';
import { Redirect, Route, useHistory } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { hammerOutline, calendarOutline, homeOutline, bookOutline, keyOutline } from 'ionicons/icons';
import Tab3 from './pages/Tab3';
import Tab4 from './pages/Tab4';
import Tab6 from './pages/Tab6';
import Login from './pages/Login';
import Tab3health from './pages/Tab3health';
import Tab3search from './pages/Tab3search';
import SchedulerRouters from './pages/scheduler/SchedulerRouters';
import Home from './pages/rwat/Home';
import GOLHome from './pages/Home';
import Prizes from './pages/gamification/Prizes';
import Tasks from './pages/gamification/Tasks';

import NIJAPI from './NIJAPI';
import { OldGamificationData as GamificationData, GameContext, GameContextProvider } from './gamification/gamification';
import { Task } from './gamification/GOALS';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';




/* Theme variables */
import './theme/variables.css';
import { LocalNotifications } from '@capacitor/local-notifications';
import FiveKeysMenu from './pages/fivekey/FiveKeysMenu';
import PrimaryAssessments from './pages/fivekey/PrimaryAssessments';
import CaseConcept from './pages/fivekey/CaseConcept';

// Fake backend data for testing, replace with real data
const dummyData: GamificationData = {
  points: 310,
  pointHistory: [50, 160, 130, 145, 150, 100],
  taskHistory: [[{
    id: 7,
    userId: 1,
    task: {
        taskId: 4,
        taskName: "Visit a Job Site",
        taskDescription: "Visit your workplace",
        rewardType: "hammer",
        rewardAmount: 25
    },
    progress: 0,
    startTime: null
  },
  {
    id: 100,
    userId: 1,
    task: {
      taskId: 100,
      taskName: "Visited a Liquor Store",
      taskDescription: "Entered ABC Beverages at 8:32PM",
      rewardType: "lightbulb",
      rewardAmount: -10
    },
    progress: 0,
    startTime: null
  },
  {
    id: 101,
    userId: 1,
    task: {
      taskId: 101,
      taskName: "Complete RWAT",
      taskDescription: "Finished RWAT",
      rewardType: "COIN",
      rewardAmount: 35
    },
    progress: 0,
    startTime: null
  }]]
};

// Empty default data
const emptyData: GamificationData = {
  points: 0,
  pointHistory: [],
  taskHistory: []
}

function toObj(str) {
  return JSON.parse(JSON.stringify(str));
}


function authenticate() {
    return NIJAPI.instance.authenticate('ethan','1234')
        .then((result) => {
          console.log("Login successful");
          return result;
        }, console.error); 
}

function TestStatus() {
  return NIJAPI.instance.fetchStatus()
        .then((result) => console.log("Coin test: " + result['coinAmount']), console.error);
}

function TestTaskList() {
  return NIJAPI.instance.fetchTaskList()
        .then((result) => console.log("Task list fetch test: " + JSON.stringify(result)), console.error); 
}

const App: React.FC = () => {
  const [ gameData, setGameData ] = useState<GamificationData>(emptyData);
  const [ taskList, setTaskList ] = useState<Task[]>([]);
  const routerRef = useRef<IonReactRouter>(null);

  const { updateGameState, points } = useContext(GameContext);

  const his = useHistory();

  function UpdateTaskList(tasks: Task[]) {
    setTaskList(tasks); 
    console.log("Backend task refresh called"); 
  }

  // Run once
  useEffect(function() {
    LocalNotifications.addListener('localNotificationActionPerformed', action => {
      if(action.actionId === "tap" && action.notification.id === 1337) // Handle tapping restriction zone warning
        routerRef.current?.history.push('/location');
    });
  }, []);

  return (
    <IonApp>
      <IonReactRouter ref={routerRef}>
        <IonTabs>
          <IonRouterOutlet>
            <GameContextProvider>
              <Route path="/home" exact={true}>
                <GOLHome />
              </Route>
              {/* <Route path="/calendar" component={Tab2} exact={true} /> */}
              <Route path="/taskswork" component={Tab3} />
              <Route path="/library" component={Tab4} />
              <Route path="/location" component={Tab6} />
              <Route path="/login" component={Login} />
              <Route path='/taskshealth' component={Tab3health} />
              <Route path='/tasks' exact={true} >
                <Tasks />
              </Route>
              <Route path='/jobs' component={Tab3search} />
              <Route path='/rwat' component={Home} />
              <Route path='/prizes' exact={true} >
                <Prizes />
              </Route>
              <Route path='/fivekeys' component={FiveKeysMenu} exact={true} />
              <Route path='/fivekeys/primary' component={PrimaryAssessments} exact={true} />
              <Route path='/fivekeys/case' component={CaseConcept} exact={true} /> 

              <SchedulerRouters />

              <Route path="/" render={() => <Redirect to="/login" />} exact={true} />
            </GameContextProvider>
          </IonRouterOutlet>
          <IonTabBar slot="bottom">
            <IonTabButton tab="home" href="/home">
                <IonIcon icon={homeOutline} />
                <IonLabel>Home</IonLabel>
            </IonTabButton>
            <IonTabButton tab="tasks" href="/tasks">
              <IonIcon icon={hammerOutline} />
              <IonLabel>Tasks</IonLabel>
            </IonTabButton>
            <IonTabButton tab="library" href="/library">
              <IonIcon icon={bookOutline} />
              <IonLabel>Library</IonLabel>
            </IonTabButton>
            <IonTabButton tab="calendar" href="/calendar">
              <IonIcon icon={calendarOutline} />
              <IonLabel>Schedule</IonLabel>
            </IonTabButton>
            <IonTabButton tab="fivekeys" href="/fivekeys">
              <IonIcon icon={keyOutline} />
              <IonLabel>Five Keys</IonLabel>
            </IonTabButton>
          </IonTabBar>
        </IonTabs>
      </IonReactRouter>
    </IonApp>
  );
}

export default App;
