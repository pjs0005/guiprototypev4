//// AI-SMS.ts
//// Facilitates connection to backend API
//// API Documentation can be found at https://docs.google.com/document/d/1u1rEMH5QpJNIraIYV367FkwDntjcWhYgMrdyp5Gka7c/view

import { File } from '@awesome-cordova-plugins/file';
import { HTTP, HTTPResponse } from '@awesome-cordova-plugins/http';
import bent from "bent";
import { Point, Polygon } from "geojson";
import qs from 'qs';
import './components/calendar/CalendarTypes';
import { CalendarEvent, Call, ChecklistItem, Meeting } from "./components/calendar/CalendarTypes";
import Checklist from './components/scheduler/ChecklistInterface';
import RWAT, { RWATAnswers } from "./RWAT";

// Define some useful types
type HttpMethod = 'get' | 'post' | 'put' | 'patch' | 'head' | 'delete' | 'options' | 'upload' | 'download';
type TaskOperation = 'finish' | 'delete';

// Define base URLs for API
const apiLocalRoot: string = 'http://192.168.1.64:8080/api/nij/ai-sms';
const apiProxyRoot: string = 'http://192.168.1.64:8010/proxy'; // To avoid CORS issues, a proxy is used when testing
const apiRoot: string = 'http://128.186.151.67:8080/api/nij/ai-sms';

// API address for scheduling backend (temporary?)
const scheduleApi: string = 'https://scheduling-api.nicholasdwest.com';
// Debug login for scheduling backend
const scheduleLogin = {
    identifier: "developer@strapi.io",
    password: "Pass123*"
};

// API address for temporary calendar API
const calendarApi: string = 'http://192.168.1.64:1337';
const calendarLogin = {
    identifier: "ethan@strapi.io",
    password: "ethan1234"
};

const locationLogin = {
    username: "jpgtest",
    password: "aisms"
};

const api = (method: bent.Options) => { return bent(apiProxyRoot, 'json', method); };

// Send a request using native HTTP plugin
async function nativeApi(path: string, method: HttpMethod, data?: { [index: string]: any }, headers?: { [index: string]: any }, baseUrl?: string)
{
    return HTTP.sendRequest((baseUrl || apiRoot) + path, {
        method: method,
        headers: headers,
        data: data,
        serializer: 'json',
        responseType: 'json',
    });
}

// API paths
const apiPaths = {
    auth: '/authentication',
    register: '/registration',

    userTrait: '/user/trait',
    userStatus: '/user/status',
    rwat: '/user/5key-assess',

    jobs: '/job/recommended-jobs',

    location: '/location/gps-data',

    taskDetail: '/task/', // Must be completed with taskId
    activeTask: '/task/active-task',

    schedule: {
        auth: '/auth/local',
        checklist: '/scheduling-checklists',
        checklistCount: '/scheduling-checklists/count',
        logs: '/scheduling-logs',
        logsCount: '/scheduling-logs/count',
        meetings: '/scheduling-meetings',
        meetingsCount: '/scheduling-meetings/count',
        phoneCalls: '/scheduling-phone-calls',
        phoneCallsCount: '/scheduling-phone-calls/count',
        tasks: '/scheduling-tasks',
        tasksCount: '/scheduling-tasks/count'
    },

    calendar: {
        auth: '/auth/local',
        events: '/calendar-events',
        calls: '/calls',
        checklists: '/checklists',
        meetings: '/meetings'
    },

    // Functions to conveniently construct API URLs
    constructed: {
        taskFetch: (taskId: number): string =>      { return apiPaths.taskDetail + taskId; },
        taskDelete: (taskId: number, operation: TaskOperation): string  =>    { return apiPaths.activeTask + '/' + taskId + '?operation=' + operation; },
        pushLocation: (lon: number, lat: number, radius?: number): string => { return apiPaths.location + `/?lon=${lon}&lat=${lat}` + (radius ? `&radius=${radius}` : '') },

        scheduleChecklists: (id: string): string => { return `${apiPaths.schedule.checklist}/${id}`; },
        scheduleLogs: (id: string): string => { return `${apiPaths.schedule.logs}/${id}`; },
        scheduleMeetings: (id: string): string => { return `${apiPaths.schedule.meetings}/${id}`; },
        schedulePhoneCalls: (id: string): string => { return `${apiPaths.schedule.phoneCalls}/${id}`; },
        scheduleTasks: (id: string): string => { return `${apiPaths.schedule.tasks}/${id}`; },
    }
};

export interface TaskInfo
{
    taskId: number,
    taskName: string,
    taskDescription: string,
    rewardType: "hammer" | "gear" | "leaf" | "lightbulb" | "COIN",
    rewardAmount: number
}

export interface Task
{
    id: number,
    userId: number,
    task: TaskInfo,
    progress: number,
    startTime: string | null
}

export interface GPSViolation
{
    id: number,
    locationName: string,
    geom: Polygon | Point
}

export interface LocationPostResponse
{
    message: string,
    gViolationCount: number,
    radius: number,
    uploadedGps: Point
    generalViolations: GPSViolation[]
}

// Modified CalendarEvent interface for pushing new events
export interface NewCalendarEvent extends Omit<CalendarEvent, 'checklist' | 'meetings' | 'calls'>
{
    checklist: string[],
    meetings: string[],
    calls: string[]
}

// AISMS is a singleton class with methods which provide an asynchronous interface to the AI-SMS API.
export default class NIJAPI
{
    private static _instance: NIJAPI;

    // Stores userId as returned by API
    userId: number = -1;

    // User email (just used for location API at the moment)
    userEmail: string = "ethan@gmail.com";

    // JWT token necessary for authorization
    private jwt: string = "";

    // Temporary, for transitional backend code
    private locationJwt: string = "";
    private scheduleJwt: string = "";
    private calendarJwt: string = "";

    private _lastLocationResponse: LocationPostResponse | undefined = undefined;
    private _knownZones: GPSViolation[] = [];

    // Return AISMS instance, or create it if it does not exist.
    static get instance(): NIJAPI
    {
        if(!NIJAPI._instance)
            NIJAPI._instance = new NIJAPI();
        
        return NIJAPI._instance;
    }

    get authenticated(): boolean
    {
        return this.jwt !== "";
    }

    get lastLocationResponse(): LocationPostResponse | undefined
    {
        return this._lastLocationResponse;
    }

    get knownZones(): GPSViolation[]
    {
        return this._knownZones;
    }

    readLocationData()
    {
        File.checkFile(File.dataDirectory, '.access')
            .then(_ => {
                console.log('Found .access');
                File.readAsText(File.dataDirectory, '.access')
                    .then(contents => {
                        console.log(contents);
                    })
                    .catch(console.error);
            })
            .catch(err => {
                console.error(err);
            })
    }

    // Write JWT and email to file so location service can send data to API
    writeAccessInfo()
    {
        return File.writeFile(File.dataDirectory, '.access', `${this.jwt}\r\n${this.userEmail}\r\n${apiLocalRoot}/`, { replace: true })
            .catch(console.error);
    }

    // Attempt login to scheduler system
    async scheduleAuth()
    {
        try
        {
            const response = await nativeApi(apiPaths.schedule.auth, 'post', scheduleLogin, undefined, scheduleApi);

            console.log(response);

            this.scheduleJwt = response.data['jwt'];

            console.log(this.scheduleJwt);

            return Promise.resolve("");
        }
        catch (err)
        {
            // console.error(err);
            // Return message returned by API
            // return Promise.reject((await err.json()).message);
            if(err.status === 400) return Promise.reject("Username or password is incorrect");
            return Promise.reject("Could not log in.");
        }
    }

    // Attempt login to calendar system
    async calendarAuth()
    {
        try
        {
            const response = await nativeApi(apiPaths.calendar.auth, 'post', calendarLogin, undefined, calendarApi);

            console.log(response);
            this.calendarJwt = response.data['jwt'];
            return Promise.resolve("");
        }
        catch (err)
        {
            if(err.status === 400) return Promise.reject("Username or password is incorrect");
            return Promise.reject("Could not log in.");
        }
    }

    // Attempt login for location
    // TODO: Remove once remote backend can be fully used
    async locationAuth()
    {
        try
        {
            const response = await nativeApi(apiPaths.auth, 'post', locationLogin, undefined, apiRoot);

            console.log(response);
            this.locationJwt = response.data['jwtToken'];
            return Promise.resolve("");
        }
        catch (err)
        {
            if(err.status === 400) return Promise.reject("Username or password is incorrect");
            return Promise.reject("Could not log in.");
        }
    }

    // Attempt log in, must successfully use this first in order to access most API.
    // If successful, resolves to an empty string.
    // Otherwise, rejects with the returned error message.
    async authenticate(user: string, pass: string): Promise<string>
    {
        let query = {
            username: user,
            password: pass
        };

        // Construct POST function
        // const request = api('POST');

        // Get JWT for location, scheduler, and calendar
        //this.locationAuth();
        //this.scheduleAuth();
        //this.calendarAuth();

        try
        {
            // Send authentication request
            // const response = await request(apiPaths.auth, query);
            const response = await nativeApi(apiPaths.auth, 'post', query);

            this.jwt = response.data['jwtToken'];
            this.userId = response.data['userId'];

            this.writeAccessInfo()
                .then(_ => {
                    this.readLocationData();
                })


            return Promise.resolve("");
        }
        catch (err)
        {
            // Return message returned by API
            // return Promise.reject((await err.json()).message);
            if(err.status === 401) return Promise.reject("Username or password is incorrect");
            return Promise.reject("Could not log in.");
        }
    }

    // Register new account. Takes an object which is directly passed to the API.
    // This object should take the form of:
    // {
    //      username: ...,
    //      password: ...,
    //      fullName: ...,
    //      gender: ...
    // }
    // If successful and autoLogIn is not false, a login will automatically be triggered next. See NIJAPI.authenticate.
    // If successful and autoLogIn is false, resolves to an empty string.
    // If unsuccessful, rejects with the returned error message.
    async register(form: object, autoLogIn: boolean = true): Promise<string>
    {
        // const request = api('POST');

        try
        {
            // Send registration request
            // await request(apiPaths.register, form);
            await nativeApi(apiPaths.register, 'post', form);

            if(autoLogIn) // Automatically log in
                return this.authenticate(form['username'], form['password']);
            
            return Promise.resolve('');
        }
        catch (err)
        {
            return Promise.reject((JSON.parse(err.error)).message);
        }
    }

    // Used to fetch user traits. Must log in first via authenticate method.
    // If successful, resolves to user traits object as defined in the API.
    // Otherwise, rejects with the returned error message.
    async fetchTraits(): Promise<string | bent.Json>
    {
        if(this.jwt === "")
            return Promise.reject("You must log in to access this feature.");

        // const request = api('GET');

        try
        {
            // Send trait fetch request
            // const response = await request(apiPaths.userTrait, undefined, { 'Authorization': "Bearer " + this.jwt });
            const response = await nativeApi(apiPaths.userTrait, 'get', {}, { 'Authorization': "Bearer " + this.jwt });

            return Promise.resolve(response.data['userTrait']);

        }
        catch (err)
        {
            return Promise.reject((JSON.parse(err.error)).message);
        }
    }

    async patchTraits(data : object): Promise<string> 
    {
        if(this.jwt === "")
            return Promise.reject("You must log in to access this feature.");

        // const request = api('GET');

        try
        {
            // Send trait fetch request
            // const response = await request(apiPaths.userTrait, undefined, { 'Authorization': "Bearer " + this.jwt });
            const response = await nativeApi(apiPaths.userTrait, 'patch', data, { 'Authorization': "Bearer " + this.jwt });

            console.log("PATCH SUCCESS"); 
            return Promise.resolve(response.data);

        }
        catch (err)
        {
            return Promise.reject((JSON.parse(err.error)).message);
        }
    }

    // Used to fetch user status. Must log in first via authenticate method.
    // If successful, resolves to user status object as defined in the API.
    // Otherwise, rejects with the returned error message.
    async fetchStatus(): Promise<string | bent.Json>
    {
        if(this.jwt === "")
            return Promise.reject("You must log in to access this feature.");

        // const request = api('GET');

        try
        {
            // Send status fetch request
            const response = await nativeApi(apiPaths.userStatus, 'get', {}, { 'Authorization': "Bearer " + this.jwt });

            return Promise.resolve(response.data['userStatus']);

        }
        catch (err)
        {
            return Promise.reject((JSON.parse(err.error)).message);
        }
    }

    // Used to recommended jobs. Must log in first via authenticate method.
    // If successful, resolves to the recommended jobs object as defined in the API
    // Otherwise, rejects with the returned error message.
    async fetchJobs(): Promise<string | Array<object>>
    {
        if(this.jwt === "")
            return Promise.reject("You must log in to access this feature.");

        // const request = api('GET');

        try
        {
            // Send job fetch request
            // const response = await request(apiPaths.jobs, undefined, { 'Authorization': "Bearer " + this.jwt });
            const response = await nativeApi(apiPaths.jobs, 'get', {}, { 'Authorization': "Bearer " + this.jwt });

            return Promise.resolve(response.data['jobListings']);

        }
        catch (err)
        {
            return Promise.reject((JSON.parse(err.error)).message);
        }
    }

    // Used to fetch user's active tasks. Must log in first via authenticate method.
    // If successful, returns the activeTasks object as defined in the API.
    // Otherwise, rejects with the returned error message.
    async fetchTaskList(): Promise<string | Array<object>>
    {
        if(this.jwt === "")
            return Promise.reject("You must log in to access this feature.");

        // const request = api('GET');

        try
        {
            // Send task list fetch request
            const response = await nativeApi(apiPaths.activeTask, 'get', {}, { 'Authorization': "Bearer " + this.jwt });

            return Promise.resolve(response.data['activeTasks']);

        }
        catch (err)
        {
            return Promise.reject((JSON.parse(err.error)).message);
        }
    }

    // Used to fetch details about a particular task. Must log in first via authenticate method.
    // If successful, returns the task object as defined in the API.
    // Otherwise, rejects with the returned error message.
    async fetchTask(taskId: number): Promise<string | any>
    {
        if(this.jwt === "")
            return Promise.reject("You must log in to access this feature.");

        // const request = api('GET');

        try
        {
            // Send task fetch request
            const response = await nativeApi(apiPaths.constructed.taskFetch(taskId), 'get', {}, { 'Authorization': "Bearer " + this.jwt });

            return Promise.resolve(response.data);

        }
        catch (err)
        {
            return Promise.reject((JSON.parse(err.error)).message);
        }
    }

    // Used to delete a task. Must log in first via authenticate method.
    // If successful, returns the deleted task object as defined in the API.
    // Otherwise, rejects with the returned error message.
    async deleteTask(taskId: number, operation: TaskOperation): Promise<string | bent.Json>
    {
        if(this.jwt === "")
            return Promise.reject("You must log in to access this feature.");

        // const request = api('DELETE');

        try
        {
            // Send task deletion request
            const response = await nativeApi(apiPaths.constructed.taskDelete(taskId, operation), 'delete', {}, { 'Authorization': "Bearer " + this.jwt });

            return Promise.resolve(response.data['deletedTask']);

        }
        catch (err)
        {
            return Promise.reject((JSON.parse(err.error)).message);
        }
    }

    // Used to push location data to the API. Must log in first via authenticate method.
    // Returns a response which includes any violations
    async pushLocationData(latitude: number, longitude: number, radius?: number): Promise<LocationPostResponse>
    {
        if(!this.authenticated)
            return Promise.reject("You must log in to access this feature.");

        // const request = api('POST');

        try
        {
            // Send location information
            // TODO: Remove specified baseUrl once the remote backend is up and running
            const response = await nativeApi(apiPaths.constructed.pushLocation(longitude, latitude, radius), 'post', {}, { 'Authorization': "Bearer " + this.jwt });
            const data: LocationPostResponse = response.data;

            // Save information from this response
            this._lastLocationResponse = data;
            data.generalViolations.forEach(v => {
                if(!this._knownZones.find(z => z.id === v.id)) this._knownZones.push(v);
            });

            return Promise.resolve(data);

        }
        catch (err)
        {
            return Promise.reject((JSON.parse(err.error)).message);
        }
    }

    // Used to push RWAT response data. Must log in first via authenticate method.
    // Automatically pulls encoded answers from RWAT object if answers is not provided.
    async pushRwat(answers?: RWATAnswers): Promise<string>
    {
        if(!answers)
            answers = RWAT.getInstance().encodeAnswers();

        console.log(answers);

        try
        {
            // Send registration request
            // await request(apiPaths.register, form);
            await nativeApi(apiPaths.rwat, 'post', answers, { 'Authorization': "Bearer " + this.jwt });
            
            return Promise.resolve('');
        }
        catch (err)
        {
            console.error(err);
            return Promise.reject((JSON.parse(err.error)).message);
        }
    }

    // Get list of checklist items
    async fetchChecklist(params?: string): Promise<any[]>
    {
        if(this.scheduleJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            // Send GET request
            const response = await nativeApi(apiPaths.schedule.checklist + params, 'get', {}, { 'Authorization': "Bearer " + this.scheduleJwt }, scheduleApi);

            return Promise.resolve(response.data);

        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Get count of checklist items
    async fetchChecklistCount(params?: string): Promise<number>
    {
        if(this.scheduleJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            // Send GET request
            const response = await nativeApi(apiPaths.schedule.checklistCount + (params || ""), 'get', {}, { 'Authorization': "Bearer " + this.scheduleJwt }, scheduleApi);

            return Promise.resolve(response.data);

        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Modify a checklist item
    async putChecklist(id: string, data: Checklist): Promise<HTTPResponse>
    {
        if(this.scheduleJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            // Send location information
            const response = await nativeApi(apiPaths.constructed.scheduleChecklists(id), 'put', data, { 'Authorization': "Bearer " + this.scheduleJwt }, scheduleApi);

            return Promise.resolve(response);

        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Get scheduling logs
    async fetchLogs(params?: string): Promise<any[]>
    {
        if(this.scheduleJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            // Send GET request
            const response = await nativeApi(apiPaths.schedule.logs + (params || ""), 'get', {}, { 'Authorization': "Bearer " + this.scheduleJwt }, scheduleApi);

            return Promise.resolve(response.data);

        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Get meetings
    async fetchMeetings(params?: string): Promise<any[]>
    {
        if(this.scheduleJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            // Send GET request
            const response = await nativeApi(apiPaths.schedule.meetings + (params || ""), 'get', {}, { 'Authorization': "Bearer " + this.scheduleJwt }, scheduleApi);

            return Promise.resolve(response.data);

        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Get phone calls
    async fetchPhoneCalls(params?: string): Promise<any[]>
    {
        if(this.scheduleJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            // Send GET request
            const response = await nativeApi(apiPaths.schedule.phoneCalls + (params || ""), 'get', {}, { 'Authorization': "Bearer " + this.scheduleJwt }, scheduleApi);

            return Promise.resolve(response.data);

        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Get events from calendar API
    // Optionally accepts an input for qs.stringify
    async getEvents(params?: any, qsOptions?: qs.IStringifyOptions)
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            // Send GET request
            const response = await nativeApi(apiPaths.calendar.events + (params ? "?" + qs.stringify(params, qsOptions) : ""), 'get', {}, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);

            let events = response.data as CalendarEvent[];
            for (const event of events) {
                event.date = new Date(event.timestamp);
            }

            return Promise.resolve(events);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Generate a request to fetch events in a specific range
    getEventRange(start: Date, end: Date)
    {
        const query = {
            _where: [
                {timestamp_gte: start.toISOString()},
                {timestamp_lte: end.toISOString()}
            ],
            _sort: 'timestamp:ASC'
        };

        return this.getEvents(query);
    }

    // Updates the chosen checklist
    async updateChecklist(id: string, value: boolean)
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            console.log(id, value);
            // Send PUT request
            const response = await nativeApi(apiPaths.calendar.checklists + '/' + id, 'put', { completed: value }, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);

            return Promise.resolve(response);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Deletes the chosen checklist
    async deleteChecklist(id: string)
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            // Send DELETE request
            const response = await nativeApi(apiPaths.calendar.checklists + '/' + id, 'delete', {}, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);

            return Promise.resolve(response);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Post a checklist item to the calendar backend
    async pushChecklist(item: ChecklistItem)
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            console.log(item);
            // Send POST request
            const response = await nativeApi(apiPaths.calendar.checklists, 'post', item, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);

            return Promise.resolve(response);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Push a list of checklist items 
    async pushChecklists(checklist: ChecklistItem[])
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            console.log(checklist);
            // Send all POST requests and wait for responses
            const responses = await Promise.all(checklist.map(async (v) => {
                return nativeApi(apiPaths.calendar.checklists, 'post', v, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);
            }));
            console.log(responses);

            return Promise.resolve(responses);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Post a meeting item to the calendar backend
    async pushMeeting(item: Meeting)
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            console.log(item);
            // Send POST request
            const response = await nativeApi(apiPaths.calendar.meetings, 'post', item, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);

            return Promise.resolve(response);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Deletes the chosen meeting
    async deleteMeeting(id: string)
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            // Send DELETE request
            const response = await nativeApi(apiPaths.calendar.meetings + '/' + id, 'delete', {}, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);

            return Promise.resolve(response);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Push a list of meeting items 
    async pushMeetings(meetings: Meeting[])
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            console.log(meetings);
            // Send all POST requests and wait for responses
            const responses = await Promise.all(meetings.map(async (v) => {
                return nativeApi(apiPaths.calendar.meetings, 'post', v, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);
            }));
            console.log(responses);

            return Promise.resolve(responses);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Post a meeting item to the calendar backend
    async pushCall(item: Call)
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            console.log(item);
            // Send POST request
            const response = await nativeApi(apiPaths.calendar.calls, 'post', item, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);

            return Promise.resolve(response);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Deletes the chosen call
    async deleteCall(id: string)
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            // Send DELETE request
            const response = await nativeApi(apiPaths.calendar.calls + '/' + id, 'delete', {}, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);

            return Promise.resolve(response);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Push a list of checklist items 
    async pushCalls(calls: Call[])
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            console.log(calls);
            // Send all POST requests and wait for responses
            const responses = await Promise.all(calls.map(async (v) => {
                return nativeApi(apiPaths.calendar.calls, 'post', v, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);
            }));
            console.log(responses);

            return Promise.resolve(responses);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Post an event to the calendar backend
    // This expects checklist, meetings, and calls to be given as a list of existing IDs
    async pushEvent(item: NewCalendarEvent)
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            console.log(item);
            // Send POST request
            const response = await nativeApi(apiPaths.calendar.events, 'post', item, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);

            return Promise.resolve(response);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Post an event to the calendar backend
    // This will handle uploading checklists, meetings, and calls given as objects to the backend
    async pushFullEvent(item: CalendarEvent)
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            let newItem = { ...item, checklist: [], meetings: [], calls: [] } as NewCalendarEvent;
            console.log(item);

            if(item.checklist.length > 0)
            {
                console.info("Pushing checklists...");
                const realChecklist = await this.pushChecklists(item.checklist);
                newItem.checklist = realChecklist.map(v => {
                    const d = v.data as ChecklistItem;
                    return d.id;
                });
            }
            if(item.meetings.length > 0)
            {
                console.info("Pushing meetings...");
                const realMeetings = await this.pushMeetings(item.meetings);
                newItem.meetings = realMeetings.map(v => {
                    const d = v.data as Meeting;
                    return d.id;
                });
            }
            if(item.calls.length > 0)
            {
                console.info("Pushing calls...");
                const realCalls = await this.pushCalls(item.calls);
                newItem.calls = realCalls.map(v => {
                    const d = v.data as Call;
                    return d.id;
                });
            }

            const response = this.pushEvent(newItem);

            return Promise.resolve(response);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }

    // Deletes the chosen event
    async deleteEvent(id: string)
    {
        if(this.calendarJwt === "")
            return Promise.reject("You must log in to access this feature.");

        try
        {
            // Send DELETE request
            const response = await nativeApi(apiPaths.calendar.events + '/' + id, 'delete', {}, { 'Authorization': "Bearer " + this.calendarJwt }, calendarApi);

            return Promise.resolve(response);
        }
        catch (err)
        {
            return Promise.reject(err);
        }
    }
}