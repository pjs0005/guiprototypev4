import React, { Component } from 'react';
import 'react-leaflet';
import { Circle, CircleMarker, GeoJSON, MapContainer, TileLayer, Tooltip } from 'react-leaflet';
import { Geolocation, Geoposition, PositionError } from '@awesome-cordova-plugins/geolocation';

import 'leaflet/dist/leaflet.css';
import './GOLMap.css';
import { Map } from 'leaflet';
import { findObjectCenter } from '../utilities';
import { sampleZones } from '../sampleObjects/sampleZones';
import Control from "react-leaflet-custom-control";
import { IonButton, IonIcon } from '@ionic/react';
import { layersOutline, layersSharp } from 'ionicons/icons';
import { Subscription, Observable } from 'rxjs';

interface GOLMapProps
{
    showOptions?: boolean,
    initialZoom?: number,
    disableTracking?: boolean,
    center?: [number, number],
    onMapCreated?: (map: Map) => void
}

interface GOLMapState
{
    center: [number, number],
    map: Map | null,
    showWarningRadius: boolean,
    showRestrictionRadius: boolean
}

function mapTilerProvider(x: any, y: any, z: any) {
    return `https://a.tile.openstreetmap.org/${z}/${x}/${y}.png`
}

const outerCircleFill = { fillColor: 'white' };
const innerCircleFill = { fillColor: 'blue' };

export default class GOLMap extends Component<GOLMapProps, GOLMapState>
{
    protected watch: Observable<Geoposition | PositionError> | null = null;
    protected geoSubscription: Subscription | null = null;

    //@ts-ignore
    constructor(props)
    {
        super(props);

        this.state = {
            center: this.props.center || [40.4653098, -86.9219977],
            map: null,
            showWarningRadius: true,
            showRestrictionRadius: true
        }

        if(this.props.disableTracking)
            this.watch = null;
        else
        {
            this.watch = Geolocation.watchPosition({ enableHighAccuracy: true });

            this.geoSubscription = this.watch.subscribe(data => {
                if((data as Geoposition).coords)
                    this.geoSuccess(data as Geoposition);
                else
                    this.geoError(data as PositionError)
            })

            // this.updateLocation();
        }
    }

    componentWillUnmount()
    {
        this.geoSubscription?.unsubscribe();
    }

    geoSuccess(pos: Geoposition)
    {
        // console.log(pos);
        this.setState( { center: [pos.coords.latitude, pos.coords.longitude] } );
    }

    geoError(err: PositionError)
    {
        console.error(err.message);
    }

    updateLocation()
    {
        console.log("Updating Location...");
        // navigator.geolocation.getCurrentPosition(this.geoSuccess.bind(this), this.geoError.bind(this), { enableHighAccuracy: true });

        Geolocation.getCurrentPosition()
            .then(this.geoSuccess)
            .catch(this.geoError)

        Geolocation.watchPosition({ enableHighAccuracy: true })

        setTimeout(this.updateLocation.bind(this), 5000);
    }

    setMap(newMap: Map)
    {
        setTimeout(() => {
            this.state.map!.invalidateSize();
        }, 500);

        this.setState({
            map: newMap
        });

        if(this.props.onMapCreated) this.props.onMapCreated(newMap);
    }

    render() {
        const {
            center,
            showWarningRadius,
            showRestrictionRadius
        } = this.state;

        return (
            // <Map provider={mapTilerProvider} center={center} zoom={15} width={365} height={350}>
            //     <Marker anchor={center}  />
            // </Map>
            <MapContainer
                className="gol-map"
                center={center}
                zoom={this.props.initialZoom || 16}
                style={{
                    height: '100%',
                    width: '100%'
                }}
                whenCreated={this.setMap.bind(this)}
            >
                <TileLayer
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.osm.org/{z}/{x}/{y}.png"
                />

                {/* Draw known restriction zones */}
                {
                    sampleZones.map(v => {
                        return (
                            <>
                                <GeoJSON data={v.geom} key={v.id}>
                                    <Tooltip>
                                        {v.locationName}
                                    </Tooltip>
                                </GeoJSON>
                                {
                                    showRestrictionRadius &&
                                    <Circle
                                        center={findObjectCenter(v.geom)}
                                        pathOptions={{
                                            stroke: true,
                                            weight: 2,
                                            opacity: 0.8,
                                            fill: false,
                                            color: '#f50057'
                                        }}
                                        radius={50}
                                        key={'c' + v.id}
                                    />
                                }
                            </>
                        );
                    })
                }

                {/* Radius for warning radius around participant */}
                {
                    showWarningRadius && 
                    <Circle
                        center={center}
                        pathOptions={{
                            stroke: true,
                            weight: 2,
                            opacity: 0.8,
                            fill: false
                        }}
                        radius={100}
                    />
                }

                {/* Marker for current position */}
                <CircleMarker
                    radius={8}
                    center={center}
                    pathOptions={{
                        stroke: true,
                        color: 'white',
                        fill: true,
                        fillColor: '#0055ff',
                        fillOpacity: 1
                    }}
                />

                {/* Control buttons */}
                {
                    this.props.showOptions &&
                    <Control position="topright">
                        <IonButton
                            shape="round"
                            fill={showRestrictionRadius ? "default" : "outline"}
                            onClick={e => {
                                this.setState({
                                    showWarningRadius: !showWarningRadius,
                                    showRestrictionRadius: !showRestrictionRadius
                                });
                            }}
                        >
                            <IonIcon icon={showRestrictionRadius ? layersSharp : layersOutline} slot='icon-only' />
                        </IonButton>
                    </Control>
                }
            </MapContainer>
        );
    }
}