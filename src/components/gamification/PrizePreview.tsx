import { IonCard, IonCardContent, IonCol, IonGrid, IonIcon, IonRow, IonTitle } from '@ionic/react';
import React, { Component } from 'react';
import { PrizeParams } from "../../sampleObjects/samplePrizes";
import './PrizePreview.css';
import './RewardTrackItem.css';




interface PrizePreviewProps 
{
    items: PrizeParams[],
    index: number
}

export default class PrizePreview extends Component<PrizePreviewProps> 
{
    render()
    {
        console.log(this.props.items, this.props.index);
        let item  = this.props.items[this.props.index]; 

        return (
            <div className="prizePreview">
                <IonTitle className='ion-text-center'>
                    Up Next
                </IonTitle>

                {/*<IonText>
                    <h5>Next prize in <span color="black">30</span> points</h5>
                </IonText>*/}

                <IonCard className="prize-preview">
                    <IonCardContent>
                        <IonGrid>
                            <IonRow>
                                <IonCol size="2">
                                    <IonIcon icon={item.icon} size="large" color="shirt"></IonIcon>
                                </IonCol>
                                <IonCol size="10">
                                    <IonTitle className="ion-text-center">
                                        {item.name}
                                    </IonTitle>
                                    <div className="ion-text-center">
                                        {item.text}
                                    </div>
                                </IonCol>
                            </IonRow>
                        </IonGrid>  
                    </IonCardContent>
                </IonCard>
            </div>
        );
    }
}

// export default class TaskItem extends Component<TaskItemProps>
// {
    // render()
    // {
    //     let task = this.props.task;

    //     return (
    //         <IonCard className="task-card" button onClick={this.props.onClick}>
    //             { task.progress > 0 && 
    //                 <IonProgressBar value={task.progress / 100} />
    //             }
                
    //             <IonCardContent>
    //                 <IonGrid>
    //                     <IonRow>
    //                         <IonCol size="10">
    //                             <IonCardTitle>
    //                                 {task.task.taskName}
    //                             </IonCardTitle>
                                
    //                             {task.task.taskDescription}
    //                         </IonCol>

    //                         <IonCol size="2">
    //                             <RewardIcon type={task.task.rewardType} amount={task.task.rewardAmount} />
    //                         </IonCol>
    //                     </IonRow>
    //                 </IonGrid>
    //             </IonCardContent>
    //         </IonCard>
    //     );
//     }
// }