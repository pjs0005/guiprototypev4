import { IonCard, IonCardContent, IonCardTitle, IonCol, IonGrid, IonProgressBar, IonRow } from '@ionic/react';
import React, { Component } from 'react';
import { Task } from '../../gamification/GOALS';
import RewardIcon from "./RewardIcon";
import './TaskItem.css';


interface TaskItemProps
{
    task: Task,
    onClick?: (event: React.MouseEvent<HTMLIonItemElement, MouseEvent>) => void
}

export default class TaskItem extends Component<TaskItemProps>
{
    render()
    {
        let task = this.props.task;

        return (
            <IonCard className="task-card" button onClick={this.props.onClick}>
                { task.completion.percentage > 0 && 
                    <IonProgressBar value={task.completion.percentage / 100} />
                }
                
                <IonCardContent>
                    <IonGrid>
                        <IonRow>
                            <IonCol size="10">
                                <IonCardTitle>
                                    {task.task.name}
                                </IonCardTitle>
                                
                                {task.task.description}
                            </IonCol>

                            <IonCol size="2">
                                <RewardIcon type='COIN' amount={task.point.value! ?? task.point.partialValue!} />
                            </IonCol>
                        </IonRow>
                    </IonGrid>
                </IonCardContent>
            </IonCard>
        );
    }
}