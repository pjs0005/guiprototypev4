import { IonSlides } from '@ionic/react';
import React, { Component } from 'react';
import { GameContext } from '../../gamification/gamification';
import { PrizeParams } from "../../sampleObjects/samplePrizes";
import './RewardTrack.css';
import RewardTrackItem from './RewardTrackItem';



interface RewardTrackProps 
{
    items: PrizeParams[],
    slideReactor: (any) => void, 
}


export default class RewardTrack extends Component<RewardTrackProps> 
{
    static contextType = GameContext;

    renderItem(item: PrizeParams, index: number) 
    {
        return(
            <RewardTrackItem
                id={item.id}
                icon={item.icon}
                top={item.top}
                name={item.name}
                text={item.text}
                earned={index <= (this.context.level - 1)}
            />
        );
    }

    render()
    {
        return (
            <IonSlides id="rewardTrack" 
                onIonSlideDidChange={e => {this.props.slideReactor(e)}}
                onIonSlidesDidLoad={e => {this.props.slideReactor(e)}}
            >

            {
                this.props.items.map((item, index) => {
                    return (
                        this.renderItem(item, index)
                    );
                })
            }
                    
            </IonSlides>
        );
    }
}