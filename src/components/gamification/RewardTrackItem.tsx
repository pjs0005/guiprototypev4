import { IonImg, IonSlide, IonCard } from '@ionic/react';
import React, { useState } from 'react';

import { IonIcon } from "@ionic/react";

import './RewardTrackItem.css';

const stampURI = "../assets/" + "ClaimedStamp.png"; 

export interface RewardTrackItemProps 
{
    id: number, 
    icon: any, 
    earned: boolean, 
    top: boolean,
    name: string,
    text: any,
}

const RewardTrackItem: React.FC<RewardTrackItemProps> = (props: RewardTrackItemProps) => {
    const [claimed, setClaimed] = useState(false);  

    function renderUnearned() {
        return (
            <IonSlide className="rewardItem">
                <IonCard className="unearnedRewardCard">
                    <h2>{props.id}</h2>
                    <h5>{props.text}</h5>
                    <IonIcon icon={props.icon} size="large" color="cash" />
                </IonCard>
            </IonSlide>
        );
    }

    function renderEarned() {

        return (
            <IonSlide className="rewardItem">
                <IonCard className="earnedRewardCard" onClick={() => {claim()}}>
                    <h2>{props.id}</h2>
                    <h5>{props.text}</h5>
                    <IonIcon icon={props.icon} size="large" color="cash" />
                </IonCard>
            </IonSlide>
        );
    }

    function renderClaimed() {
        return (
            <IonSlide className="rewardItem">
                <IonCard className="earnedRewardCard">
                    <h2>{props.id}</h2>
                    <h5>{props.text}</h5>
                    <IonIcon icon={props.icon} size="large" color="cash" />
                    <IonImg src={stampURI} className="stamp"/>
                </IonCard>
            </IonSlide>
        );
    }

    function renderStamp() {
        return (<IonImg src={stampURI} className="stamp"/>);
    }

    function claim() {
        setClaimed(true); 
    }

    
    let toReturn = renderUnearned(); 

    if (props.earned)
        toReturn = renderEarned(); 

    if (props.earned && claimed)
        toReturn = renderClaimed(); 

    return(toReturn)
}

export default RewardTrackItem;