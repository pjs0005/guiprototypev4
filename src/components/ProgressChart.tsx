import React from 'react';
import { Radar } from 'react-chartjs-2'
import "./ProgressChart.css";

interface FiveKeyProgress
{
    maxLevel: number,
    work: number,
    health: number,
    social: number,
    relationships: number,
    coping: number
}

interface ProgressChartProps
{
    progress: FiveKeyProgress
}

const ProgressChart: React.FC<ProgressChartProps> = (props) => {
    const { progress } = props;
    const data = {
        labels: ['Work', 'Health', 'Social', 'Relationships', 'Coping'],
        datasets: [
            {
                label: 'Progress Level',
                data: [progress.work, progress.health, progress.social, progress.relationships, progress.coping],
                pointBackgroundColor: [
                    'green',
                    'grey',
                    'tan',
                    'darkred',
                    'darkblue'
                ],
                borderWidth: 1
            }
        ]
    }

    return (
        <div className="progressChartContainer">
            <Radar data={data} options={{
                maintainAspectRatio: false,
                plugins: {
                    legend: {
                        display: false
                    }
                },
                scales: {
                    r: {
                        max: progress.maxLevel,
                        min: 0,
                        
                        ticks: {
                            stepSize: 1
                        },

                        angleLines: {
                            display: true,
                            lineWidth: 1
                        },

                        pointLabels: {
                            display: true,
                            font: {
                                size: 12
                            }
                        }
                    }
                }
            }}/>

        </div>
    );
};

export default ProgressChart;