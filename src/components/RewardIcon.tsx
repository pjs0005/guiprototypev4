import React from 'react';
import { bulb, cog, hammer, leaf, logoUsd } from 'ionicons/icons';
import { IonIcon, IonText } from '@ionic/react';
import './RewardIcon.css';

const styling = {
    "hammer": {
        icon: hammer
    },
    "gear": {
        icon: cog
    },
    "leaf": {
        icon: leaf
    },
    "lightbulb": {
        icon: bulb
    },
    "COIN": {
        icon: logoUsd
    }
}

interface RewardIconProps {
    type: "hammer" | "gear" | "leaf" | "lightbulb" | "COIN",
    amount?: number
}

const RewardIcon: React.FC<RewardIconProps> = (props) => {

    return (
        <div className={`reward-icon ${props.type}`} >
            <IonIcon icon={styling[props.type].icon} size="large" />
            
            {props.amount &&  
                <IonText>
                    {props.amount}
                </IonText>  
            }
        </div>
    );
};

export default RewardIcon;