export interface Call{
    name:string;
    topic:string;
    callStatus:string;
    sender:string;
    receiver:string;
}