interface SchedulingLog {
    id:number;
    dueTime:string;
    status:string;
    caseWorker:string;
    description:string;
    month:string;
    day:string;
    year:number;
    date:string;
    checklistCount:number;
  }

  
  export default SchedulingLog