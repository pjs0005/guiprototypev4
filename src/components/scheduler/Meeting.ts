export interface Meeting{
    name:string;
    type:string;
    location:string;
    time:string;
    date:string;
}