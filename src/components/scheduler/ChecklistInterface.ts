interface Checklist {
    id:number;
    completed:boolean;
    date:string;
    name:string;
  }

  export default Checklist