import React from 'react';
import { IonIcon, IonItem, IonLabel, IonText } from '@ionic/react';
import { briefcaseOutline, calendarClearOutline, chatboxOutline } from 'ionicons/icons'
import { EventAvailableOutlined, EventBusyOutlined, HourglassEmptyOutlined } from '@material-ui/icons';
import SchedulingLog from '../scheduler/SchedulingLogInterface';
import Checklist from '../scheduler/ChecklistInterface';
import { Meeting } from '../scheduler/Meeting';
import { Call } from '../scheduler/Call';
import { CalendarEvent } from './CalendarTypes'
import './EventItem.css';

interface EventItemProps
{
    event: CalendarEvent
}

export function getEventIcon(event: CalendarEvent)
{
    switch(event.type)
    {
        case 'meeting':
            return chatboxOutline;
        case 'job':
            return briefcaseOutline;
        default:
            return calendarClearOutline;
    }
}

export function getStatusIcon(event: CalendarEvent)
{
    
    switch(event.approval)
    {
        case 'approved':
            return <EventAvailableOutlined className="calendarEventApprovalIcon" />;
        case 'pending':
            return <HourglassEmptyOutlined className="calendarEventApprovalIcon" />;
        case 'rejected':
            return <EventBusyOutlined className="calendarEventApprovalIcon" />;
    }
}

const EventItem: React.FC<EventItemProps> = ({ event }) => {
    return (
        <IonItem button lines="none" className={`calendarEvent event${event.approval} event${event.type}`} routerLink={`/calendar/event/${event.id}`}>
            <IonIcon className="calendarEventTypeIcon" icon={getEventIcon(event)} slot="start" />

            <IonLabel>
                <IonText className="calendarEventName">
                    { event.name }
                </IonText>
                <br />
                <IonText className="calendarEventSub">
                    at { event.date?.toLocaleTimeString([], {hour: 'numeric', minute: '2-digit'}) }{event.location.length > 0 ? `, ${event.location}` : ''}
                </IonText>
            </IonLabel>

            <div slot="end">
                { getStatusIcon(event) }
            </div>
        </IonItem>
    );
}

export default EventItem;