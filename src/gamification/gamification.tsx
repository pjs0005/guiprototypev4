import React, { useCallback, useReducer } from 'react';
import { Task as OldTask } from '../NIJAPI';
import { sampleGOALSTasks } from '../sampleObjects/sampleTasksNew';
import { Task } from './GOALS';
import { GameAction, GameFlag, handleGameFlag } from './tasks';

export interface OldGamificationData
{
    points: number,             // Current number of points
    pointHistory: number[],     // Points gained in the past n days, where pointHistory[0] is the current day, pointHistory[1] is yesterday, etc.
    taskHistory?: OldTask[][],     // Tasks completed in the past n days, where taskHistory[0] is the list of tasks completed today, taskHistory[1] is yesterday, etc.
}

export interface GamificationData
{
    points: number,                     // Current number of points
    level: number,                      // Current level
    pointHistory: number[],            // Points gained in the past n days, where pointHistory[0] is the current day, pointHistory[1] is yesterday, etc.
    taskHistory: Task[][],       // Tasks completed in the past n days, where taskHistory[0] is the list of tasks completed today, taskHistory[1] is yesterday, etc.
    activeTask?: Task             // Current task being completed in the app
    userId?: number,                    // ID of logged in user
    username?: string,                  // Username of logged in user
    tasks: Task[],                // Current tasks for this user
    stateFlags: Record<string, any>     // Flags used for some arbitrary state info, e.g. debug flags
}

export interface GameContextValue extends GamificationData
{
    updateGameState: React.Dispatch<Partial<GamificationData>>,
    doGameEvent: <T extends GameFlag>(action: GameAction<T>) => void
}

export const GameContext = React.createContext<GameContextValue>({
    points: 0,
    level: 0,
    pointHistory: [],
    taskHistory: [],
    tasks: [],
    stateFlags: {},
    updateGameState: v => { console.trace(`Got updateGameState using default state! This has no effect.`) },
    doGameEvent: v => { console.trace(`Got doGameEvent using default state! This has no effect.`) }
});

export type GameStateAction = Partial<GamificationData> & {
    preApply?: (GamificationData) => Partial<GamificationData> | null,      // Chain another reducer before this one
    postApply?: (GamificationData) => Partial<GamificationData> | null      // Chain another reducer after this one
};

// Apply new values from action to state
function gameReducer(state: GamificationData, action: GameStateAction): GamificationData
{
    console.log("old", state);
    // console.log("new", { ...state, ...action });
    let newState = {...state};
    if(action.preApply)
    {
        newState = {...newState, ...action.preApply(newState)};
        action.preApply = undefined;
    }

    newState = {...newState, ...action};

    if(action.postApply)
    {
        newState = {...newState, ...action.postApply(newState)};
        action.postApply = undefined;
    }

    console.log("new", newState);
    return newState;
}

export const GameContextProvider: React.FC = (props) => {
    // Set up reducer for gamification data
    const [state, updateState] = useReducer(gameReducer, {
        pointHistory: [],
        taskHistory: [],
        stateFlags: {
            rwatQuickSubmit: true,
            rwatAlwaysFinish: true
        },
        points: 0,
        level: 0,
        tasks: sampleGOALSTasks
    });

    // Set up game event handler
    const gameEvent = useCallback(<T extends GameFlag>(action: GameAction<T>) => {
        updateState({
            preApply: (state: GamificationData) => handleGameFlag(state, action)
        });
    }, [updateState]);

    // Set up context value
    const gameState: GameContextValue = {
        ...state,
        updateGameState: updateState,
        doGameEvent: gameEvent
    };

    return (
        <GameContext.Provider value={gameState}>
            { props.children }
        </GameContext.Provider>
    );
}