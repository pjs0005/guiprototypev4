export interface TaskDescription
{
    name: string,                               // Task name
    id: number,                                 // ID of task for user
    listId: number,                             // ID of task in backend
    type: 'meeting' | 'activity' | 'session',   // Task category
    description: string,                        // Task description
    fivekey: 'coping' | 'work' | 'social' | 'relationships' | 'health' | null,  // Relevant key in 5-Key Model
    date: string
}

export interface TaskCreatorInfo
{
    type: 'casemanager' | 'participant' | 'system',     // Task origin
    uid: number                                         // Unique identifier for creator
}

export interface TaskCompletionInfo
{
    type: 'binary' | 'partial',     // How task completion is handled, i.e. completed/not completed or partial completion allowed
    status: boolean,                // Task completion
    ongoing: boolean,               // If true, the task has been started
    percentage: number              // Completion percentage
}

export interface TaskTimeInfo
{
    timeLimited: boolean            // Does this task have a deadline?
    from: string | null,            // Start datetime of task
    to: string | null               // End datetime of task
}

export interface TaskRegularity
{
    on: boolean,                            // Is this task reassigned regularly?
    freq: 'daily' | 'weekly' | 'monthly',   // How often is it reassigned?
    from: string | null,                    // When does the task start repeating?
    to: string | null                       // When does the task stop repeating?
}

export interface TaskRedoing
{
    allowed: boolean,           // Can this task be redone?
    treatedSeparate: boolean,   // Should each attempt at this task be treated as a separate task?
    donePlace: string           // Info on how the task should behave when repeated, e.g. restart or start at last finish point
}

export interface TaskPoints
{
    value?: number | null,          // Flat point value
    maxvalue?: number | null,       // Max value of this task
    partialValue?: number | null    // Partial completion value
}

export interface Task
{
    user_id: number,
    associated_goals_ui: string,
    mandatory: boolean,

    task: TaskDescription,
    created_by: TaskCreatorInfo,
    completion: TaskCompletionInfo,
    time: TaskTimeInfo,
    regularity: TaskRegularity,
    redoing: TaskRedoing,
    point: TaskPoints
}