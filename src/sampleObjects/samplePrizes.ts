import { cash, timeOutline } from "ionicons/icons";


export interface PrizeParams {
	id: number,
	icon: string,
	claimed: boolean,
	top: boolean,
	name: string,
	text: any
}

export const samplePrizes: PrizeParams[] = [
	{
		"id": 1,
		"icon" : timeOutline,
		"claimed": false,
		"top": true,
		"name": "Time extension",
		"text" : "Incentive pass +2 hours"
	},
	{
		"id": 2,
		"icon" : timeOutline,
		"claimed": false,
		"top": true,
		"name": "Time extension",
		"text" : "Errand pass +30 minutes"
	},
	{
		"id": 3,
		"icon" : timeOutline,
		"claimed": false,
		"top": true,
		"name": "Time extension",
		"text" : "Incentive pass +2 hours"
	},
	{
		"id": 4,
		"icon" : timeOutline,
		"claimed": false,
		"top": true,
		"name": "Time extension",
		"text" : "Extension of curfew to 9:00pm"
	},
	{
		"id": 5,
		"icon" : timeOutline,
		"claimed": false,
		"top": true,
		"name": "Time extension",
		"text" : "Incentive pass +2 hours"
	},
	{
		"id": 6,
		"icon" : cash,
		"claimed": false,
		"top": true,
		"name": "Keeping it clean",
		"text" : "Free drug screen"
	}
];