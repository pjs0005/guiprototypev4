import { Task } from '../gamification/GOALS';

export const sampleGOALSTasks: Task[] = [
    {
      "user_id": 338996,
      "task": {
        "name": "Avoid restricted areas",
        "id": 1,
        "listId": 12,
        "type": "activity",
        "description": "Stay away from restricted areas for 7 days (a week)",
        "fivekey": "coping",
        "date": "2021-08-09 00:06:05"
      },
      "associated_goals_ui": "geolocation",
      "created_by": {
        "type": "casemanager",
        "uid": 213
      },
      "completion": {
        "type": "binary",
        "status": false,
        "ongoing" : false,
        "percentage": 10
      },
      "time": {
        "timeLimited": false,
        "from": "2021-08-09 00:06:05",
        "to": "2021-08-10 00:06:05"
      },
      "regularity": {
        "on": false,
        "freq": "daily",
        "from": "2021-08-09 00:06:05",
        "to": "2021-08-10 00:06:05"
      },
      "redoing": {
        "allowed": true,
        "treatedSeparate": true,
        "donePlace": "?"
      },
      "mandatory": true,
      "point": {
        "value": 100,
        "partialValue": null
      }
    },
     // i just copied and pasted the above one here. 
    {
      "user_id": 338996,
      "task": {
        "name": "Complete RWAT",
        "id": 2,
        "listId": 101,
        "type": "activity", //{meeting|activity|session}
        "description": "Complete the Reentry Well-Being Assessment Tool",
        "fivekey": "coping",
        "date": new Date().toISOString()
      },
      "associated_goals_ui": "RWAT",
  
      //Who initiates this task;
      //Task is given by the system automatically based on some conditions or the participant just starts it manually? '
      //(Case manager,  Participant, System (or System Admin))
  
      "created_by": {
        "type": "casemanager",
        "uid": 213
      },
      "completion": {
        "type": "binary",
        "status": false,
        "ongoing" : true,
        "percentage": 50
      },
      "time": {
        "timeLimited": false,
        "from": null,
        "to": null
      },
  
      //Tasks is periodic and given regularly or one time event or Randomly given?
      "regularity": {
        "on": false,
        "freq": "daily",
        "from": "2021-08-09 00:06:05",
        "to": "2021-08-10 00:06:05"
      },
  
      //redoing is allowed?--restart entirely or where they haven finished?)
      "redoing": {
        "allowed": true,
        "treatedSeparate": true,
        "donePlace": "?"
      },
  
      "mandatory": true,
  
      "point": {
        "maxvalue": 100,
        "partialValue": 50
      }
    }
  ]