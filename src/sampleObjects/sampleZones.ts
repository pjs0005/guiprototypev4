import { GPSViolation, LocationPostResponse } from '../NIJAPI';

export const sampleZones: GPSViolation[] = [
    {
        id: 134,
        locationName: "Amberleigh Village Park",
        geom: {
            type: "Polygon",
            coordinates: [
                [
                    [
                        -86.9219977,
                        40.4653098
                    ],
                    [
                        -86.9219965,
                        40.4651945
                    ],
                    [
                        -86.9218591,
                        40.4651945
                    ],
                    [
                        -86.9218556,
                        40.4652666
                    ],
                    [
                        -86.921916,
                        40.4653098
                    ],
                    [
                        -86.9219977,
                        40.4653098
                    ]
                ]
            ]
        }
    },
    {
        id: 135,
        locationName: "Amberleigh Village Park",
        geom: {
            type: "Polygon",
            coordinates: [
                [
                    [
                        -86.9217845,
                        40.465445
                    ],
                    [
                        -86.9218319,
                        40.4653891
                    ],
                    [
                        -86.9217158,
                        40.465327
                    ],
                    [
                        -86.9217039,
                        40.4653243
                    ],
                    [
                        -86.921672,
                        40.4653846
                    ],
                    [
                        -86.9217845,
                        40.465445
                    ]
                ]
            ]
        }
    },
    {
        id: 136,
        locationName: "Amberleigh Village Park",
        geom: {
            type: "Polygon",
            coordinates: [
                [
                    [
                        -86.9217347,
                        40.465463
                    ],
                    [
                        -86.9216424,
                        40.4654144
                    ],
                    [
                        -86.9215997,
                        40.465445
                    ],
                    [
                        -86.9215689,
                        40.4654639
                    ],
                    [
                        -86.9215713,
                        40.4654801
                    ],
                    [
                        -86.9216684,
                        40.4655288
                    ],
                    [
                        -86.9217347,
                        40.465463
                    ]
                ]
            ]
        }
    }
];

export const sampleGPSResponse: LocationPostResponse = {
    message: "gps uploaded",
    gViolationCount: sampleZones.length,
    radius: 100,
    uploadedGps: {
        type: "Point",
        coordinates: [
            -86.9219977,
            40.4653098
        ]
    },
    generalViolations: sampleZones
};