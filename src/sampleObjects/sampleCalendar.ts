import { CalendarEvent } from '../components/calendar/CalendarTypes';

export const sampleEvents: CalendarEvent[] = [
    {
        id: '1',
        name: 'Meeting with Dr. P',
        description: 'Weekly meeting with Dr. P\n\nHeld online via Skype until further notice.',
        date: new Date(2021, 4, 7, 14, 55, 0, 0),
        timestamp: new Date(2021, 4, 7, 14, 55, 0, 0).toISOString(),
        approval: 'approved',
        type: 'meeting',
        location: 'Skype',
        checklist: [
            {
                id: '0',
                name: "Go to website.com before meeting",
                completed: false
            }
        ],
        meetings: [
            {
                id: '1',
                name: "Dr. P",
                type: "skype",
                location: "Skype",
                time: "2:55 PM"
            }
        ],
        calls: [
            {
                id: '1',
                name: "Dr. P",
                topic: "Weekly Meeting",
                callStatus: "answered",
                sender: "Dr. P",
                receiver: "Ethan"
            }
        ]
    },
    {
        id: '2',
        name: 'Job Interview',
        description: "Interview at Big Town BBQ",
        date: new Date(2021, 4, 7, 17, 0, 0, 0),
        timestamp: new Date(2021, 4, 7, 17, 0, 0, 0).toISOString(),
        approval: 'pending',
        type: 'job',
        location: 'Big Town BBQ',
        checklist: [
            {
                id: '0',
                name: "Bring copy of resume",
                completed: true
            }
        ],
        meetings: [
            {
                id: '2',
                name: "Blaine",
                type: "meeting",
                location: "Big Town BBQ",
                time: "5:00 PM"
            }
        ],
        calls: []
    },
    {
        id: '3',
        name: 'Session',
        description: "5-Key Model Session",
        date: new Date(2021, 4, 7, 18, 0, 0, 0),
        timestamp: new Date(2021, 4, 7, 18, 0, 0, 0).toISOString(),
        approval: 'rejected',
        type: 'other',
        location: 'Office',
        checklist: [],
        meetings: [
            {
                id: '3',
                name: "Session",
                type: "meeting",
                location: "Office",
                time: "6:00 PM"
            }
        ],
        calls: []
    }
];